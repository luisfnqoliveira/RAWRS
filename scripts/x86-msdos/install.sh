#!/bin/bash

echo ""
echo "Installing MS-DOS Target"
echo "========================"
echo ""

ROOTDIR=$PWD
COMMONDIR=packages/common
INSTALLDIR=packages/x86-msdos

if [ ! -f ${ROOTDIR}/scripts/x86-msdos/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - checking prerequisites"

CHECK="texi2pdf bison flex gcc make"
good=1

for binary in ${CHECK}
do
  if ! hash ${binary} 2>/dev/null; then
    echo " - ERROR: must install ${binary}"
    good=0
  fi
done

if [ ${good} -eq 0 ]; then
  echo " - ERROR: prerequisites needed"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - all prerequisites found"
echo ""

source $PWD/scripts/x86-msdos/versions.sh

echo "1. GNU binutils"
echo "---------------"
echo ""

echo " - using version ${BINUTILS_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/binutils-${BINUTILS_VERSION} ]; then
  echo " - downloading binutils ${BINUTILS_VERSION}"
  wget -q -nc http://ftp.gnu.org/gnu/binutils/binutils-${BINUTILS_VERSION}.tar.xz -O ${ROOTDIR}/${COMMONDIR}/binutils-${BINUTILS_VERSION}.tar.xz
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking binutils-${BINUTILS_VERSION}.tar.xz"
  tar xf ${ROOTDIR}/${COMMONDIR}/binutils-${BINUTILS_VERSION}.tar.xz
  cd ../..
else
  echo " - binutils already found with version ${BINUTILS_VERSION}"
fi

echo ""
echo "2. DJGPP Headers and Utilities (djgpp-djcrx, djgpp-djlsr)"
echo "---------------------------------------------------------"
echo ""

echo " - using version ${DJGPP_DJCRX_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION} ]; then
  echo " - creating ${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}/src/stub/stub.h ]; then
  echo " - downloading djgpp-djcrx ${DJGPP_DJCRX_VERSION}"
  wget -q -nc http://www.delorie.com/pub/djgpp/current/v2/djcrx205.zip -O ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}/djcrx205.zip
  cd ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}
  echo " - unpacking djcrx205.zip"
  unzip -q -o djcrx205.zip
  cd ../../..
else
  echo " - djgpp-djcrx already found with version ${DJGPP_DJCRX_VERSION}"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}/utils/buildmak.mak ]; then
  echo " - downloading djgpp-djlsr ${DJGPP_DJCRX_VERSION}"
  wget -q -nc http://www.delorie.com/pub/djgpp/current/v2/djlsr205.zip -O ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}/djlsr205.zip
  cd ${ROOTDIR}/${INSTALLDIR}/djgpp-djcrx-${DJGPP_DJCRX_VERSION}
  echo " - unpacking djlsr205.zip"
  unzip -q -o djlsr205.zip
  cd ../../..
else
  echo " - djgpp-djlsr already found with version ${DJGPP_DJCRX_VERSION}"
fi

echo ""
echo "3. GNU C Compiler (gcc)"
echo "-----------------------"
echo ""
echo " - using version ${GCC_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/gcc-${GCC_VERSION} ]; then
  echo " - downloading gcc ${GCC_VERSION}"
  wget -q -nc https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.xz -O ${ROOTDIR}/${COMMONDIR}/gcc-${GCC_VERSION}.tar.xz
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking gcc-${GCC_VERSION}.tar.xz"
  tar xf ${ROOTDIR}/${COMMONDIR}/gcc-${GCC_VERSION}.tar.xz
  cd ../..
else
  echo " - gcc already found with version ${GCC_VERSION}"
fi

echo ""
echo "4. ISL Library (for gcc)"
echo "------------------------"
echo ""
echo " - using version ${ISL_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/isl-${ISL_VERSION} ]; then
  echo " - downloading isl ${ISL_VERSION}"
  wget -q -nc https://gcc.gnu.org/pub/gcc/infrastructure/isl-${ISL_VERSION}.tar.bz2 -O ${ROOTDIR}/${COMMONDIR}/isl-${ISL_VERSION}.tar.bz2
  cd ${ROOTDIR}/${INSTALLDIR}
  tar xf ${ROOTDIR}/${COMMONDIR}/isl-${ISL_VERSION}.tar.bz2
  cd ../..
else
  echo " - isl already found with version ${ISL_VERSION}"
fi

echo ""
echo "5. CWSDPMI (DPMI Server)"
echo "------------------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/cwsdpmi ]; then
  echo " - downloading cwsdpmi from FreeDOS git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone https://gitlab.com/FreeDOS/util/cwsdpmi
  mkdir -p ${ROOTDIR}/assets/static/x86-msdos
  cp cwsdpmi/BIN/CWSDPMI.EXE ${ROOTDIR}/assets/static/x86-msdos/CWSDPMI.EXE
  cd ${ROOTDIR}
else
  echo " - cwsdpmi source repository already exists"
fi

echo ""
echo "6. Em-DOSBox (MS-DOS emulator)"
echo "------------------------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/em-dosbox ]; then
  echo " - downloading em-dosbox from git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone https://github.com/warpcoil/em-dosbox
  cd ${ROOTDIR}
else
  echo " - em-dosbox source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/em-dosbox
if ! git cat-file -e ${EM_DOSBOX_VERSION}; then
  echo " - fetching updates to em-dosbox"
  git pull
else
  echo " - revision ${EM_DOSBOX_VERSION} found"
fi

echo " - checking out ${EM_DOSBOX_VERSION}"
git reset --hard ${EM_DOSBOX_VERSION} --quiet
cd ../../..

echo ""
echo "7. The Curse of CGA (Example)"
echo "-----------------------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/the-curse-of-cga ]; then
  echo " - downloading the-curse-of-cga from git sources"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone https://github.com/wilkie/the-curse-of-cga
  cd ../..
else
  echo " - the-curse-of-cga source repository already exists"
fi

cd ${ROOTDIR}/${INSTALLDIR}/the-curse-of-cga
if ! git cat-file -e ${THE_CURSE_OF_CGA_VERSION}; then
  echo " - fetching updates to the-curse-of-cga"
  git pull
else
  echo " - revision ${THE_CURSE_OF_CGA_VERSION} found"
fi

echo ""
echo "Done."
