#!/bin/bash

HEADER="Building Em-DOSBox"
TARGET=x86-msdos
PACKAGE=dosbox

# The host target we are building
HOST="i586-pc-msdosdjgpp"

source $PWD/scripts/common/include.sh

SRCDIR=$PWD/packages/x86-msdos/em-dosbox
BUILDJSDIR=packages/x86-msdos/em-dosbox-js

INSTALLDIR=assets/js/targets/x86-msdos/dosbox

echo " - installing/updating MS-DOS packages"
./scripts/x86-msdos/install.sh &> ${ROOTDIR}/packages/x86-msdos/install-during-dosbox.log

if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - copying em-dosbox repository to the ${BUILDJSDIR} directory"
  cp -r ${SRCDIR} ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/src/dosbox.wasm ]; then
  echo " - configuring Em-DOSBox"
  cd ${ROOTDIR}/${BUILDJSDIR}
  # We configure with a none host to get through the configuration
  # Otherwise, it tries to run HTML files which obviously does not work.
  ./autogen.sh &> ${ROOTDIR}/${BUILDJSDIR}/1-autogen.log
  emconfigure ./configure --enable-wasm --disable-opengl --host=none-none-none &> ${ROOTDIR}/${BUILDJSDIR}/2-configure-initial.log
  cp config.h config.h.cached
  LDFLAGS="-s MODULARIZE=1 -s EXPORT_NAME=DOSBox" emconfigure ./configure --enable-wasm --enable-es6 --disable-opengl --host=none-none-none &> ${ROOTDIR}/${BUILDJSDIR}/3-configure.log
  cp config.h.cached config.h

  echo " - patching src/Makefile"
  cd ${ROOTDIR}/${BUILDJSDIR}/src
  sed "s/ -s EXPORT_ES6=1//" -i Makefile

  echo " - building Em-DOSBox"
  cd ${ROOTDIR}/${BUILDJSDIR}
  # Strangely, this one generates only a JavaScript target so we just run 'make'
  run "make" "${ROOTDIR}/${BUILDJSDIR}/4-make.log"

  # Patching the JavaScript module output
  echo " - patching resulting src/dosbox.js"
  cd ${ROOTDIR}/${BUILDJSDIR}/src

  # Patches findEventTarget so it never attaches an event to the window / <html> tag.
  sed "s/function findEventTarget(target){/function findEventTarget(target){if(target==2){return document.body.querySelector('#dosbox-canvas');}/" -i dosbox.js

  # We don't want DOSBox updating the title
  sed "s/document.title=title/title=title/" -i dosbox.js
else
  echo " - using existing build of Em-DOSBox (remove src/dosbox.wasm)"
fi

cd ${ROOTDIR}

if [ ! -d ${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript files"
  mkdir -p ${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript files"
fi

mkdir -p ${ROOTDIR}/${INSTALLDIR}

# Install dosbox.js/dosbox.wasm
cd ${ROOTDIR}/${BUILDJSDIR}

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/dosbox.js ]; then
  echo " - creating ${INSTALLDIR}/dosbox.js"
  cp src/dosbox.js ${ROOTDIR}/${INSTALLDIR}/dosbox.js
else
  echo " - creating ${INSTALLDIR}/dosbox.js (exists already)"
fi

if [ ! -f ${ROOTDIR}/${INSTALLDIR}/dosbox.wasm ]; then
  echo " - creating ${INSTALLDIR}/dosbox.wasm"
  cp src/dosbox.wasm ${ROOTDIR}/${INSTALLDIR}/dosbox.wasm
else
  echo " - creating ${INSTALLDIR}/dosbox.wasm (exists already)"
fi

echo " - done!"
