# MS-DOS Toolchain

These scripts build a native and a JavaScript (emscripten) version of a complete
toolchain targetting 16 and 32-bit MS-DOS. This consists of binutils (an assembler
and linker via `as` and `ld` respectively), a C runtime and standard library via
djgpp, djcrx, and a C compiler via `gcc` (and its subcomponents `cc1`, `collect2`,
and `cpp` for its various stages).

## Scripts

* `build-binutils.sh` - Builds binutils. Creates `as`, `ld`, `readelf`, and `objdump` JavaScript workers.
* `build-gcc.sh` - Builds `gcc`, `g++`, `cc1`, `cc1plus` `cpp`, `gcc-ar`, `gcc-nm` and `collect2` along with JavaScript workers.
* `build-djgpp-djcrx.sh` - Builds the runtime and libc needed to run applications.
* `build-dosbox.sh` - Builds the full RISC-V emulator for JavaScript.

The `gcc` and `g++` that is built has forking disabled and announces the commands it wants to run.
In this form, the underlying system is responsible for delegating the commands to the workers built in other
steps such as `as`, `ld,` and `cc1` in the case of a C project compile. Generally, the output files
left by the final step provided by `gcc` is the resulting output file and every other file is a temporary
intermediate that may or may not be kept.
