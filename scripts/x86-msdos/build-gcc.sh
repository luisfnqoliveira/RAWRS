#!/bin/bash

HEADER="Building MS-DOS gcc"
TARGET=x86-msdos
PACKAGE=gcc

# The host target we are building
HOST="i586-pc-msdosdjgpp"

source $PWD/scripts/common/include.sh

NATIVEDIR=${ROOTDIR}/${UTILSDIR}/gcc-install-native
SRCDIR=$PWD/packages/x86-msdos/gcc-${GCC_VERSION}
DJCRXBUILDDIR=packages/x86-msdos/djgpp-djcrx-${DJGPP_DJCRX_VERSION}-bootstrap
BUILDNATIVEDIR=packages/x86-msdos/gcc-${GCC_VERSION}-build-native
BUILDDIR=packages/x86-msdos/gcc-${GCC_VERSION}-build
BUILDJSDIR=packages/x86-msdos/gcc-${GCC_VERSION}-js

INSTALLDIR=assets/js/targets/x86-msdos/gcc

echo " - installing/updating MS-DOS packages"
./scripts/x86-msdos/install.sh &> ${ROOTDIR}/packages/x86-msdos/install-during-gcc.log

# First we need a bootstrapped djcrx for the native build
DJCRXSRCDIR=${ROOTDIR}/packages/x86-msdos/djgpp-djcrx-${DJGPP_DJCRX_VERSION}

if [ ! -d ${ROOTDIR}/${DJCRXBUILDDIR} ]; then
  echo " - copying empty djcrx build to ${DJCRXBUILDDIR}"
  cp -r ${DJCRXSRCDIR} ${ROOTDIR}/${DJCRXBUILDDIR}
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/stubify ]; then
  cd ${ROOTDIR}/${DJCRXBUILDDIR}
  echo " - compiling stubs"
  run "make -f cross/makefile stub CFLAGS=${CFLAGS}" "${ROOTDIR}/${DJCRXBUILDDIR}/native-make.log"
  cd ${ROOTDIR}
else
  echo " - not compiling stubs. already installed into ${UTILSDIR}/usr/bin/stubify"
fi

echo " - installing /usr/${HOST}/sys-include"
install -dm 0755 "${ROOTDIR}/${UTILSDIR}/usr/${HOST}"/sys-include
cp -r "${ROOTDIR}/${DJCRXBUILDDIR}/lib"/*     "${ROOTDIR}/${UTILSDIR}/usr/${HOST}"/lib/.

echo " - installing /usr/${HOST}/lib"
install -dm 0755 "${ROOTDIR}/${UTILSDIR}/usr/${HOST}"/lib
cp -r "${ROOTDIR}/${DJCRXBUILDDIR}/include"/* "${ROOTDIR}/${UTILSDIR}/usr/${HOST}"/sys-include/

for _file in stubedit stubify; do
  if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${_file} ]; then
    echo " - installing /usr/bin/${_file}"
    install -Dm 0755 "${ROOTDIR}/${DJCRXBUILDDIR}/$_file" "${ROOTDIR}/${UTILSDIR}/usr/bin/$_file"
  fi
done

for file in copying copying.dj copying.lib readme.1st; do
    install -Dm644 "${ROOTDIR}/${DJCRXBUILDDIR}/${file}" ${ROOTDIR}/${UTILSDIR}/usr/share/licenses/djgpp-djcrx/${file}
done

echo " - patching gcc"

cd ${SRCDIR}

if [ ! -f ${SRCDIR}/patching.log ]; then
  # build the lto plugin
  patch -s -Np0 < ${PATCHDIR}/lto.patch &> ${SRCDIR}/patching.log

  # Other DJGPP related changes
  patch -s -Np1 < ${PATCHDIR}/gcc-djgpp.diff &>> ${SRCDIR}/patching.log

  # Patch driver-i386
  cd ${SRCDIR}/gcc/config/i386
  sed "s/defined(__GNUC__)/\!defined\(__EMSCRIPTEN__\) \&\& defined\(__GNUC__\)/" -i driver-i386.c
  cd ${SRCDIR}
else
  echo " - patches already believed to be applied (remove patching.log to reapply)"
fi

if [ ! -f ${SRCDIR}/isl/configure ]; then
  echo " - linking isl directory to gcc source tree"
  ln -fs "../isl-0.18" ${SRCDIR}/isl
else
  echo " - using existing isl source tree within gcc source tree"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${BUILDNATIVEDIR} ]; then
  echo " - creating ${BUILDNATIVEDIR} directory"
  mkdir -p ${ROOTDIR}/${BUILDNATIVEDIR}
else
  echo " - warning: using existing build directory at ${BUILDNATIVEDIR}"
fi

# Set native flags
export CPPFLAGS="${CPPFLAGS} -O2"

# What are we building
_bootstrap_languages=c,c++
_build_languages=c,c++

if [ ! -f ${NATIVEDIR}/bin/gcc ]; then
  echo " - configuring a native gcc ${GCC_VERSION}"
  cd ${ROOTDIR}/${BUILDNATIVEDIR}

  ../gcc-${GCC_VERSION}/configure --prefix=${NATIVEDIR} --enable-languages=${_bootstrap_languages} --disable-multilib --enable-__cxa_atexit --disable-plugin --disable-libsanitizer &> ${ROOTDIR}/${BUILDNATIVEDIR}/0-native-gcc-configure.log

  echo " - building a native gcc ${GCC_VERSION}"
  run "make bootstrap" "${ROOTDIR}/${BUILDNATIVEDIR}/1-native-make.log"

  echo " - installing the native gcc ${GCC_VERSION}"
  make install &> ${ROOTDIR}/${BUILDNATIVEDIR}/2-native-install.log
else
  echo " - using the native gcc ${GCC_VERSION} installed in ${UTILSDIR}/gcc-install-native"
fi

# Add native gcc to our path to take over for system gcc
export PATH=${NATIVEDIR}/bin:${ROOTDIR}/${UTILSDIR}/usr/bin:$PATH

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${BUILDDIR} ]; then
  echo " - creating ${BUILDDIR} directory for the native gcc ${GCC_VERSION} for msdos targets"
  mkdir -p ${ROOTDIR}/${BUILDDIR}
else
  echo " - warning: using existing build directory at ${BUILDDIR}"
fi

if [ ! -f ${ROOTDIR}/${UTILSDIR}/usr/bin/${HOST}-gcc ]; then
  echo " - linking djgpp system libraries into the prefix as /dev/env/DJGPP"

  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/${HOST}/dev/env/DJDIR
  ln -fs ../../../sys-include ${ROOTDIR}/${UTILSDIR}/usr/${HOST}/dev/env/DJDIR/include

  echo " - configuring a native gcc ${GCC_VERSION} for msdos targets"
  cd ${ROOTDIR}/${BUILDDIR}

  ../gcc-${GCC_VERSION}/configure --prefix=/usr --libexecdir=/usr/lib \
    --target="${HOST}" \
    --with-sysroot=${ROOTDIR}/${UTILSDIR}/usr/${HOST} \
    --enable-languages=${_build_languages} \
    --enable-shared --enable-static \
    --enable-threads=no --with-system-zlib --with-isl \
    --enable-lto --disable-libgomp \
    --disable-multilib --enable-checking=release \
    --disable-libstdcxx-pch \
    --enable-libstdcxx-filesystem-ts \
    --disable-install-libiberty &> ${ROOTDIR}/${BUILDDIR}/0-configure.log

  echo " - building a native gcc ${GCC_VERSION} for msdos targets"
  run "make all" "${ROOTDIR}/${BUILDDIR}/1-make.log"

  echo " - installing the native gcc ${GCC_VERSION} for msdos targets"
  make install DESTDIR=${ROOTDIR}/${UTILSDIR} &> ${ROOTDIR}/${BUILDDIR}/2-install.log

  # strip manually, djgpp libs spew errors otherwise
  echo " - stripping libraries"
  rm -rf ${ROOTDIR}/${BUILDDIR}/3-strip.log
  touch ${ROOTDIR}/${BUILDDIR}/3-strip.log
  for fn in cc1 cc1plus cc1obj cc1objplus f951 lto1 lto-wrapper gnat1; do
    file=${ROOTDIR}/${UTILSDIR}/usr/lib/gcc/${HOST}/${GCC_VERSION}/$fn
    if [ -f $file ]; then
      strip $file &>> ${ROOTDIR}/${BUILDDIR}/3-strip.log
    else
      echo " - warning: cannot strip: $file not found"
    fi
  done

  ${HOST}-strip -v -g ${ROOTDIR}/${UTILSDIR}/usr/lib/gcc/${HOST}/${GCC_VERSION}/*.a &>> ${ROOTDIR}/${BUILDDIR}/3-strip.log
  ${HOST}-strip -v -g ${ROOTDIR}/${UTILSDIR}/usr/${HOST}/lib/*.a &>> ${ROOTDIR}/${BUILDDIR}/3-strip.log

  # for compatibility
  ln -fs ${HOST}-gcc "${ROOTDIR}"/"${UTILSDIR}"/usr/bin/${HOST}-cc

  # remove unnecessary files
  rm -rf "${ROOTDIR}"/${UTILSDIR}/usr/share/{man/man7,info,locale}
  rm -rf "${ROOTDIR}"/${UTILSDIR}/usr/share/gcc-$pkgver/python
  rm -rf "${ROOTDIR}"/${UTILSDIR}/usr/lib/gcc/${HOST}/$pkgver/include-fixed
  rm -f "${ROOTDIR}"/${UTILSDIR}/usr/lib*/libcc1.*
else
  echo " - using the native gcc ${GCC_VERSION} installed in ${UTILSDIR}"
fi

# Now build the JavaScript version
if [ ! -d ${ROOTDIR}/${BUILDJSDIR} ]; then
  echo " - creating ${BUILDJSDIR} directory for the JavaScript gcc ${GCC_VERSION} for msdos targets"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}
else
  echo " - warning: using existing build directory at ${BUILDJSDIR}"
fi

if [ ! ${EMSDK} ]; then
  echo " - initializing emcripten"
  source "$ROOTDIR/packages/emsdk/emsdk_env.sh" &> ${ROOTDIR}/${BUILDJSDIR}/0-emscripten-initialize.log
else
  echo " - using existing activated emcripten at ${EMSDK}"
fi

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc.o ]; then
  echo " - linking djgpp system libraries into the prefix as /dev/env/DJGPP"

  mkdir -p ${ROOTDIR}/${UTILSDIR}/usr/dev/env/DJDIR
  ln -fs ../../../${HOST}/sys-include ${ROOTDIR}/${UTILSDIR}/usr/dev/env/DJDIR/include

  # It is important to nuke ftw.h (and other things) so that gcc does not use it
  EMFIXES="include/ftw.h include/sys/prctl.h"

  cd ${ROOTDIR}/${BUILDJSDIR}
  for nukeheader in ${EMFIXES}; do
    if [ -f ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} ]; then
      echo " - nuking emscripten's system ${nukeheader} header file"
      cp ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} $(basename ${nukeheader})
      rm ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader}
    fi
  done

  # Removes the symlink
  echo " - removing the isl source tree link for the JavaScript build"
  rm -f ${SRCDIR}/isl

  echo " - configuring a JavaScript gcc ${GCC_VERSION} for msdos targets"
  cd ${ROOTDIR}/${BUILDJSDIR}

  # Compile options

  PROFILING_OPTS="-O2"

  # Uncomment to gain stack traces
  #PROFILING_OPTS="${PROFILING_OPTS} -g --profiling-funcs"

  export CFLAGS="-DHAVE_PSIGNAL -DHAVE_LIMITS_H=1 -DHAVE_STRSIGNAL=1 -DHAVE_DECL_SBRK=1 -DHAVE_DECL_STRSIGNAL=1 -DHAVE_SYS_RESOURCE_H=1 -DHAVE_SYS_TIMES_H=1 -DHAVE_STDLIB_H=1 -DHAVE_UNISTD_H=1 -DHAVE_STRING_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_FCNTL_H=1 -m32 ${PROFILING_OPTS} -s ALLOW_MEMORY_GROWTH=1 -s USE_PTHREADS=0"
  export CPPFLAGS="${CFLAGS} -I${ROOTDIR}/packages/system/include"
  export CXXFLAGS=${CPPFLAGS}
  export LDFLAGS="-Wl,--no-check-features -s USE_PTHREADS=0 -L${ROOTDIR}/packages/system/lib ${ROOTDIR}/packages/system/lib/libgmp.a ${ROOTDIR}/packages/system/lib/libmpfr.a ${ROOTDIR}/packages/system/lib/libmpc.a"
  export CPLUS_INCLUDE_PATH=${ROOTDIR}/packages/system/include

  cd ${ROOTDIR}/${BUILDJSDIR}
  emconfigure ../gcc-${GCC_VERSION}/configure --prefix=/ --libexecdir=/usr/lib \
    --target="${HOST}" \
    --with-sysroot=${ROOTDIR}/${UTILSDIR}/usr/${HOST} \
    --with-gmp=${ROOTDIR}/packages/system \
    --with-mpfr=${ROOTDIR}/packages/system \
    --with-mpc=${ROOTDIR}/packages/system \
    --with-isl=${ROOTDIR}/packages/system \
    --with-zlib=${ROOTDIR}/packages/system \
    --enable-languages=${_build_languages} \
    --disable-multilib \
    --disable-plugin \
    --disable-libsanitizer \
    --disable-libstdc++ \
    --disable-shared \
    --enable-static \
    --disable-threads \
    --enable-lto \
    --disable-libstdcxx-pch \
    --disable-checking \
    --enable-libstdcxx-filesystem-ts \
		--disable-libatomic \
		--disable-libmudflap \
		--disable-libssp \
		--disable-libquadmath \
		--disable-libgomp \
		--disable-nls \
		--disable-bootstrap \
    --disable-install-libiberty &> ${ROOTDIR}/${BUILDJSDIR}/1-configure.log

  SUBPROJECTS="gcc libcody"

  for subproject in ${SUBPROJECTS}
  do
    echo " - configuring JavaScript ./${subproject} path to prepare for the build"
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/${subproject}
    cd ${ROOTDIR}/${BUILDJSDIR}/${subproject}
    emconfigure ${SRCDIR}/${subproject}/configure --srcdir=${SRCDIR}/${subproject} --cache-file=./config.cache --with-system-zlib --prefix=/ --libexecdir=/usr/lib --with-sysroot=${ROOTDIR}/${UTILSDIR}/usr/${HOST} --with-gmp=${ROOTDIR}/packages/system --with-mpfr=${ROOTDIR}/packages/system --with-mpc=${ROOTDIR}/packages/system --with-isl=${ROOTDIR}/packages/system --with-zlib=${ROOTDIR}/packages/system --enable-shared --enable-static --enable-threads=no --with-system-zlib --with-isl --enable-lto --disable-multilib --disable-libcody --disable-c++tools --disable-checking --disable-libstdcxx-pch --enable-libstdcxx-filesystem-ts --disable-bootstrap --disable-libssp --disable-libgomp --disable-nls --disable-install-libiberty --enable-languages=c,lto "--program-transform-name=s&^&${HOST}-&" --disable-option-checking --build=x86_64-pc-linux-gnu --host=x86_64-pc-linux-gnu --target=${HOST} &> ${ROOTDIR}/${BUILDJSDIR}/2-configure-${subproject}.log
    cd ..
  done

  # Replace the emscripten headers
  cd ${ROOTDIR}/${BUILDJSDIR}
  for nukeheader in ${EMFIXES}; do
    if [ ! -f ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader} ]; then
      echo " - restoring emscripten's system ${nukeheader} header file"
      cp $(basename ${nukeheader}) ${ROOTDIR}/packages/emsdk/upstream/emscripten/cache/sysroot/${nukeheader}
    fi
  done

  echo " - building (initial pass... will fail) a JavaScript gcc ${GCC_VERSION} for msdos targets"
  cd ${ROOTDIR}/${BUILDJSDIR}
  run "emmake make all-gcc CXX=em++ CFLAGS=\"${CFLAGS}\" CPPFLAGS=\"${CPPFLAGS}\" LDFLAGS=\"${LDFLAGS}\" PTHREAD_CFLAGS=\"\"" "${ROOTDIR}/${BUILDJSDIR}/3-make.log"

  echo " - copying over built ./gcc to JavaScript build path"
  mkdir -p ${ROOTDIR}/${BUILDJSDIR}/gcc/build
  find ${ROOTDIR}/${BUILDDIR}/gcc/build -type f ! -name "*.*" -exec cp --preserve=mode {} ${ROOTDIR}/${BUILDJSDIR}/gcc/build/. \;
  find ${ROOTDIR}/${BUILDJSDIR}/gcc/build -type f ! -name "*.*" -exec touch -d "+24 hour" {} \;

  echo " - copying over native generator and bootstrap binaries to JavaScript build path"

  COPIES="fixincludes/fixincl gcc/xgcc gcc/cc1 build-x86_64-pc-linux-gnu/fixincludes/fixincl gcc/as"

  for fixup in ${COPIES}
  do
    rm -f ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    mkdir -p ${ROOTDIR}/${BUILDJSDIR}/$(dirname "${fixup}")
    echo " - copying ${fixup} over from the native build"
    cp --preserve=mode ${ROOTDIR}/${BUILDDIR}/${fixup} ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/${fixup}
    touch -d "+24 hour" ${ROOTDIR}/${BUILDJSDIR}/${fixup}
  done

  for subproject in ${SUBPROJECTS}
  do
    echo " - building JavaScript ./${subproject} path"
    cd ${ROOTDIR}/${BUILDJSDIR}/${subproject}
    run "emmake make" "${ROOTDIR}/${BUILDJSDIR}/4-make-${subproject}.log"
    cd ..
  done
else
  echo " - using existing built JavaScript gcc ${GCC_VERSION} for msdos targets"
fi

cd ${ROOTDIR}

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR} for JavaScript workers"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
else
  echo " - installing to existing ${INSTALLDIR} for JavaScript workers"
fi

EMSCRIPTEN_WASM_OPTS="-s WASM=1 -s NO_EXIT_RUNTIME=0 -lworkerfs.js -lfs.js -s FORCE_FILESYSTEM=1 -s ASSERTIONS=1 ${PROFILING_OPTS}"

# Create the new gcc entry
if [ ! -f ${SRCDIR}/gcc/gcc-bare.c ]; then
  echo " - creating gcc-bare.c"
  cp ${SRCDIR}/gcc/gcc.c ${SRCDIR}/gcc/gcc-bare.c
  cd ${SRCDIR}
  patch -N -p1 < ${PATCHDIR}/gcc-bare.patch &> ${SRCDIR}/patching.log
else
  echo " - creating gcc-bare.c (exists already)"
fi

if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc-bare.o ]; then
  echo " - creating gcc-bare.o"
  cd ${ROOTDIR}/${BUILDJSDIR}/gcc
  COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/4-make-gcc.log | grep "\-o gcc.o" | sed "s/gcc\./gcc-bare./g"`
  echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh
  chmod +x ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh
  source ${ROOTDIR}/${BUILDJSDIR}/gcc/build-gcc-bare.sh

  if [ ! -f ${ROOTDIR}/${BUILDJSDIR}/gcc/gcc-bare.o ]; then
    echo " - ERROR: could not build gcc-bare.o"
    exit 1
  fi
else
  echo " - creating gcc-bare.o (exists already)"
fi

# Link
BINARIES="cpp gcc-ar gcc-nm gcc-ranlib cc1 cc1plus xgcc xg++"

for binary in ${BINARIES}
do
  # xgcc and xg++ must be just "gcc" and "g++"
  dest=${binary}
  if [[ ${binary} == x* ]]; then
    dest=`echo ${binary} | cut -c2-`
  fi

  if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js ]; then
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js"
    cd ${ROOTDIR}/${BUILDJSDIR}/gcc
    rm -f ${binary}
    run "emmake make ${binary}" "${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log"
    context=0
    while [[ `cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "\-o ${binary} " -A${context}` == *\\ ]]; do
      context=$[$context + 1]
    done

    # Creates *-bare.js
    COMMAND=`cat ${ROOTDIR}/${BUILDJSDIR}/make-${binary}.log | grep "\-o ${binary} " -A${context} | sed "s; gcc\.o; gcc-bare.o;g" | sed "s;-o ${binary};${EMSCRIPTEN_WASM_OPTS} -o ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js;"`
    echo ${COMMAND} > ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh
    chmod +x ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh
    source ${ROOTDIR}/${BUILDJSDIR}/gcc/build-${binary}.sh

    if [ ! -f ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ]; then
      echo " - ERROR: cannot make JavaScript ${dest}"
      exit 1
    fi

    # Creates the worker itself
    cat ${ROOTDIR}/assets/js/browserfs.min.js ${ROOTDIR}/lib/pre-worker.js ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}-bare.js ${ROOTDIR}/lib/post-worker.js > ${ROOTDIR}/${INSTALLDIR}/${HOST}-${dest}.js
  else
    echo " - creating ${INSTALLDIR}/${HOST}-${dest}.js (exists already)"
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
