#!/bin/bash

echo ""
echo "Installing ARM64 Linux Target"
echo "============================="
echo ""

ROOTDIR=$PWD
COMMONDIR=packages/common
INSTALLDIR=packages/arm64-linux

if [ ! -f ${ROOTDIR}/scripts/arm64-linux/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - checking prerequisites"

CHECK="texi2pdf bison flex gcc make"
good=1

for binary in ${CHECK}
do
  if ! hash ${binary} 2>/dev/null; then
    echo " - ERROR: must install ${binary}"
    good=0
  fi
done

if [ ${good} -eq 0 ]; then
  echo " - ERROR: prerequisites needed"
  echo ""
  echo "Failed."
  exit 1
fi

echo " - all prerequisites found"
echo ""

source $PWD/scripts/arm64-linux/versions.sh

echo "1. GNU binutils"
echo "---------------"
echo ""

echo " - using version ${BINUTILS_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/binutils-${BINUTILS_VERSION} ]; then
  echo " - downloading binutils ${BINUTILS_VERSION}"
  wget -q -nc http://ftp.gnu.org/gnu/binutils/binutils-${BINUTILS_VERSION}.tar.xz -O ${ROOTDIR}/${COMMONDIR}/binutils-${BINUTILS_VERSION}.tar.xz
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking binutils-${BINUTILS_VERSION}.tar.xz"
  tar xf ${ROOTDIR}/${COMMONDIR}/binutils-${BINUTILS_VERSION}.tar.xz
  cd ../..
else
  echo " - binutils already found with version ${BINUTILS_VERSION}"
fi

echo ""
echo "2. GNU C Compiler (gcc)"
echo "-----------------------"
echo ""
echo " - using version ${GCC_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/gcc-${GCC_VERSION} ]; then
  echo " - downloading gcc ${GCC_VERSION}"
  wget -q -nc https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.xz -O ${ROOTDIR}/${COMMONDIR}/gcc-${GCC_VERSION}.tar.xz
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - unpacking gcc-${GCC_VERSION}.tar.xz"
  tar xf ${ROOTDIR}/${COMMONDIR}/gcc-${GCC_VERSION}.tar.xz
  cd ../..
else
  echo " - gcc already found with version ${GCC_VERSION}"
fi

echo ""
echo "3. ISL Library (for gcc)"
echo "------------------------"
echo ""
echo " - using version ${ISL_VERSION}"

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/isl-${ISL_VERSION} ]; then
  echo " - downloading isl ${ISL_VERSION}"
  wget -q -nc https://gcc.gnu.org/pub/gcc/infrastructure/isl-${ISL_VERSION}.tar.bz2 -O ${ROOTDIR}/${COMMONDIR}/isl-${ISL_VERSION}.tar.bz2
  cd ${ROOTDIR}/${INSTALLDIR}
  tar xf ${ROOTDIR}/${COMMONDIR}/isl-${ISL_VERSION}.tar.bz2
  cd ../..
else
  echo " - isl already found with version ${ISL_VERSION}"
fi

echo ""
echo "Done."
