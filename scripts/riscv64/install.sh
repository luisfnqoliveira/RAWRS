#!/bin/bash

if [ ! ${RAWRS_INSTALLING} ]; then
  echo ""
  echo "Installing RISC-V Target"
  echo "========================"
  echo ""
fi

ROOTDIR=$PWD
INSTALLDIR=packages/riscv64

if [ ! -f ${ROOTDIR}/scripts/riscv64/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  echo ""
  echo "Failed."
  exit 1
fi

if [ ! ${RAWRS_INSTALLING} ]; then
  echo " - checking prerequisites"

  CHECK="texi2pdf bison flex gcc make"
  good=1

  for binary in ${CHECK}
  do
    if ! hash ${binary} 2>/dev/null; then
      echo " - ERROR: must install ${binary}"
      good=0
    fi
  done

  if [ ${good} -eq 0 ]; then
    echo " - ERROR: prerequisites needed"
    echo ""
    echo "Failed."
    exit 1
  fi

  echo " - all prerequisites found"
  echo ""
fi

# Pull in version environment variables
source $PWD/scripts/riscv64/versions.sh

echo "1. Ace Editor RISC-V Highlighter"
echo "--------------------------------"
echo ""

if [ ${ROOTDIR}/lib/mode-assembly_riscv.js -nt ${ROOTDIR}/assets/js/ace-builds/src-noconflict/mode-assembly_riscv.js ]; then
  cp ${ROOTDIR}/lib/mode-assembly_riscv.js ${ROOTDIR}/assets/js/ace-builds/src-noconflict/.
  echo " - mode-assembly_riscv.js copied into ace-builds/src-noconflict"
else
  echo " - mode-assembly_riscv.js is unchanged"
fi

echo ""
echo "2. GNU RISC-V Toolchain"
echo "-----------------------"
echo ""

cd ${ROOTDIR}
if [ ! -d ${ROOTDIR}/${INSTALLDIR} ]; then
  echo " - creating ${INSTALLDIR}"
  mkdir -p ${ROOTDIR}/${INSTALLDIR}
fi

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/riscv-gnu-toolchain ]; then
  echo " - cloning ${RISCV_TOOLCHAIN_URL}"
  cd ${ROOTDIR}/${INSTALLDIR}
  git clone ${RISCV_TOOLCHAIN_URL} &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-0-git-clone.log
  cd riscv-gnu-toolchain
  echo " - checking out ${RISCV_TOOLCHAIN_VERSION}"
  git reset --hard ${RISCV_TOOLCHAIN_VERSION} &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-1-git-reset.log
  echo " - initializing submodules"
  git submodule init &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-2-git-submodule-init.log
  echo " - cloning riscv-binutils"
  git submodule update riscv-binutils &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-3-git-update-riscv-binutils.log
  echo " - cloning riscv-gdb"
  git submodule update riscv-gdb &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-4-git-update-riscv-gdb.log
  echo " - cloning riscv-gcc"
  git submodule update riscv-gcc &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-5-git-update-riscv-gcc.log
  echo " - cloning riscv-glibc"
  git submodule update riscv-glibc &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-gnu-toolchain-6-git-update-riscv-glibc.log
  cd ..
else
  echo " - riscv-gnu-toolchain already exists with version ${RISCV_TOOLCHAIN_VERSION}"
fi

echo ""
echo "3. RISC-V newlib (libc)"
echo "-----------------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/riscv-newlib ]; then
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - cloning ${RISCV_NEWLIB_URL}"
  git clone ${RISCV_NEWLIB_URL} &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-newlib-0-git-clone.log

  echo " - checking out ${RISCV_NEWLIB_VERSION}"
  git reset --hard ${RISCV_NEWLIB_VERSION} &> ${ROOTDIR}/${INSTALLDIR}/install-riscv-newlib-1-git-reset.log
else
  echo " - newlib source already found with version ${RISCV_NEWLIB_VERSION}"
fi

echo ""
echo "4. TinyEMU (RISC-V Emulator)"
echo "----------------------------"
echo ""

if [ ! -d ${ROOTDIR}/${INSTALLDIR}/tinyemu ]; then
  cd ${ROOTDIR}/${INSTALLDIR}
  echo " - cloning TinyEMU from ${RISCV_TINYEMU_URL}"
  git clone ${RISCV_TINYEMU_URL} &> ${ROOTDIR}/${INSTALLDIR}/install-tinyemu-0-git-clone.log
else
  echo " - TinyEMU already exists (remove TinyEMU directory)"
fi

# Return to the base path
cd ${ROOTDIR}

if [ ! ${RAWRS_INSTALLING} ]; then
  echo ""
  echo "Done."
fi
