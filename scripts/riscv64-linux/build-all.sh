#!/bin/bash

echo ""
echo "Building 64-bit RISC-V for Linux Target"
echo "======================================="

SYSTEMDIR=packages/riscv64/system

RAWRS_COUNTER=0 ./scripts/riscv64-linux/build-binutils.sh
RAWRS_COUNTER=1 ./scripts/riscv64-linux/build-gdb.sh
RAWRS_COUNTER=2 INSTALLDIR=${SYSTEMDIR} ./scripts/common/build-gmp.sh
RAWRS_COUNTER=3 INSTALLDIR=${SYSTEMDIR} ./scripts/common/build-mpfr.sh
RAWRS_COUNTER=4 INSTALLDIR=${SYSTEMDIR} ./scripts/common/build-mpc.sh
RAWRS_COUNTER=5 INSTALLDIR=${SYSTEMDIR} ./scripts/common/build-isl.sh
RAWRS_COUNTER=6 INSTALLDIR=${SYSTEMDIR} ./scripts/common/build-zlib.sh
RAWRS_COUNTER=7 ./scripts/riscv64-linux/build-gcc.sh
RAWRS_COUNTER=8 ./scripts/riscv64-linux/build-glibc.sh
RAWRS_COUNTER=9 ./scripts/riscv64-linux/build-riscvpk.sh
RAWRS_COUNTER=10 ./scripts/riscv64-linux/build-linux.sh

echo ""
