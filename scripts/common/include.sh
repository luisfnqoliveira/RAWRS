#!/bin/bash

if [ -z "${NICE+xxx}" ]; then
  NICE=1
fi

run() {
  cmd=$1
  log=$2
  append=$3

  if [ ${append} ]; then
    if [ ${NICE} ]; then
      echo ""
      eval ${cmd} |& tee -a "${log}" | grep --line-buffered -ohe "\(\W\|\/\)\w\+\.cc\?\(pp\)\?\(\W\|$\)" | sed -u 's/^./\x1b[A\x1b[K\x1b[1K   ✓ [ /' | sed -u 's/\(\s\|\.\)\?$/ ]/'
      echo -e "\e[A\e[K\e[1K\e[A"
    elif [ ${VERBOSE} ]; then
      eval ${cmd} |& tee -a "${log}"
    else
      eval ${cmd} &>> "${log}"
    fi
  else
    if [ ${NICE} ]; then
      echo ""
      eval ${cmd} |& tee "${log}" | grep --line-buffered -ohe "\(\W\|\/\)\w\+\.cc\?\(pp\)\?\(\W\|$\)" | sed -u 's/^./\x1b[A\x1b[K\x1b[1K   ✓ [ /' | sed -u 's/\(\s\|\.\)\?$/ ]/'
      echo -e "\e[A\e[K\e[1K\e[A"
    elif [ ${VERBOSE} ]; then
      eval ${cmd} |& tee "${log}"
    else
      eval ${cmd} &> "${log}"
    fi
  fi
}

ROOTDIR=$PWD

echo ""
if [ ${RAWRS_COUNTER} ]; then
  HEADER="${RAWRS_COUNTER}. ${HEADER}"
  echo "${HEADER}"
  echo "${HEADER}" | sed 's/./-/g'
else
  echo "${HEADER}"
  echo "${HEADER}" | sed 's/./=/g'
fi
echo ""

if [ ! -f ${ROOTDIR}/scripts/common/include.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

if [ -f ${ROOTDIR}/scripts/${TARGET}/versions.sh ]; then
  source $PWD/scripts/${TARGET}/versions.sh
fi

PATCHDIR=${ROOTDIR}/patches/${TARGET}/${PACKAGE}
UTILSDIR=utils/${TARGET}
