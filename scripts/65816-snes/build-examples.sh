#!/bin/bash

HEADER="Building Examples"
TARGET=65816-snes
PACKAGE=examples

source $PWD/scripts/common/include.sh

SRCDIR=${ROOTDIR}/packages/${TARGET}
INSTALLDIR=assets/files/${TARGET}/examples

PATHS="classickong snes-sdk/snesc"

for path in ${PATHS}; do
  project=$(basename ${path})

  if [ ! -d ${ROOTDIR}/${INSTALLDIR}/${project} ]; then
    echo " - creating ${INSTALLDIR}/${project}"
    mkdir -p ${ROOTDIR}/${INSTALLDIR}/${project}
  fi

  # Copy files
  echo " - copying ${path}/*.{c,h,asm,map,dat}"
  cp ${SRCDIR}/${path}/*.{c,h,asm,map,dat} ${ROOTDIR}/${INSTALLDIR}/${project}/. 2>/dev/null
  if [ -f ${SRCDIR}/${path}/LICENSE ]; then
    echo " - copying ${path}/LICENSE"
    cp ${SRCDIR}/${path}/LICENSE ${ROOTDIR}/${INSTALLDIR}/${project}/.
  fi
done

# Return to the base path
cd ${ROOTDIR}

# Done.
echo " - done!"
