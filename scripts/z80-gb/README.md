# GameBoy (z80ish) Toolchain

These scripts build the GBDK/SDCC toolchain for building C-based and Assembly
GameBoy projects to both a native and JavaScript (emscripten) versions.

## Scripts

* `build-gputils.sh` - Builds GPUTILS, which are tools targetting microarchitectures.
* `build-sdcc.sh` - Builds `sdcc`, `sdasgb`, `sdldgb` which can compile, assembly, and link GameBoy projects respectively.
* `build-gbdk.sh` - Builds `lcc` which is the main compiler entrypoint for SDCC targetting GameBoy (and similar) architectures.
* `build-visualboy.sh` - Builds Visual Boy Advance natively and as a web worker libretro core.

## Processing

The `sdcc` compiler and the `lcc` front-end both system/popen out to subprocesses to do their job.
Therefore, our JavaScript workers need to coordinate with each other when appropriate.

In order to do this, we patch `sdcc` via `SDCCmain.c` to tell us what the preprocessor command is.
Then we run that. And then we re-run the sdcc process and have it read in the preprocessed file.
This way, we avoid forking `sdcpp` which is not as easily done in emscripten land since IPC is
not very readily available... and neither is multiple heaps. (See `sdcc-dispatch.patch`)

The `lcc` app works a lot like `gcc` in that it precomputes the entire call chain and then would
normally just system out to each subcommand (such as the compiler, assembler, linker, etc). So,
we just run `lcc` to get the list of commands and then run each ourselves. This is how we support
`gcc` so it is rather trivial to do so here as well. (See `gbdk-lcc.patch`)

Visual Boy Advance is complex, but we can support it as a generic emulator by wrapping its
libretro core library with our libretro shell. The libretro core has a function that emulates until
it has a single frame of video. (`retro_run()`) We invoke that each frame and transmit pixel data
across from the web worker. Not being able to control a Canvas in a web worker does mean that
this operation is very expensive. OffscreenCanvas will help in the near future.

The audio works similarly. We queue until we have around 3,000 audio
samples for each channel. The sample rate, here, is around 30k, so this is around a tenth of a
second latency for reasonable performance. The buffer copy is expensive, but web browsers and
web audio both need to catch up considerably to make that more efficient, but a shared buffer
and/or the ability to queue audio from a worker would help. Maybe in the near future.
