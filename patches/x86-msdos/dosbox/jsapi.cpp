#include "mem.h"
#include "regs.h"

extern "C" {
  int dosbox_mem_readw(unsigned int address) {
    return mem_readw(address);
  }

  void dosbox_mem_writew(unsigned int address, unsigned int value) {
    mem_writew(address, value);
  }

  unsigned int dosbox_read_registers() {
    return cpu_regs.regs[0].dword[0];
  }

  int dosbox_write_registers() {
  }

  int dosbox_step() {
  }

  int dosbox_set_breakpoint() {
  }

  int dosbox_clear_breakpoint() {
  }
}
