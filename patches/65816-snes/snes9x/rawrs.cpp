#include <libretro.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <emscripten.h>

extern "C" {
    void get_registers(void* data) {
    }

    void set_registers(void* data) {
    }

    uint8_t read_memory8(uint64_t ptr) {
        return 0;
    }

    void write_memory8(uint64_t ptr, uint8_t value) {
    }

    uint16_t read_memory16(uint64_t ptr) {
        return 0;
    }

    void write_memory16(uint64_t ptr, uint16_t value) {
    }

    uint32_t read_memory32(uint64_t ptr) {
        return 0;
    }

    void write_memory32(uint64_t ptr, uint32_t value) {
    }
}
