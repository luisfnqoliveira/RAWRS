#!/bin/bash

ROOTDIR=$PWD

if [ ! -f ${ROOTDIR}/scripts/riscv64/versions.sh ]; then
  echo " - ERROR: need to run this script from the rawrs project directory"
  exit 1
fi

EMSCRIPTEN_VERSION=3.1.2

echo "Emscripten"
echo "=========="

if [ ! -d packages/emsdk ]; then
  cd packages
  git clone https://github.com/emscripten-core/emsdk.git
  cd emsdk
  ./emsdk install ${EMSCRIPTEN_VERSION}
  ./emsdk activate ${EMSCRIPTEN_VERSION}
  cd ..
else
  echo "emscripten already exists. delete the emsdk directory to reinstall"
fi

echo "BrowserFS"
echo "========="

cd ${ROOTDIR}
if [ ! -f ${ROOTDIR}/assets/js/browserfs.min.js ]; then
  echo " - downloading assets/js/browserfs.min.js"
  wget -q -nc https://github.com/jvilk/BrowserFS/releases/download/v1.4.3/browserfs.min.js -O ${ROOTDIR}/assets/js/browserfs.min.js
else
  echo " - assets/js/browserfs.min.js already exists"
fi

if [ ! -f ${ROOTDIR}/assets/js/browserfs.min.js.map ]; then
  echo " - downloading assets/js/browserfs.min.js.map"
  wget -q -nc https://github.com/jvilk/BrowserFS/releases/download/v1.4.3/browserfs.min.js.map -O ${ROOTDIR}/assets/js/browserfs.min.js.map
else
  echo " - assets/js/browserfs.min.js.map already exists"
fi

echo ""
echo "Ace Editor"
echo "=========="

if [ ! -d assets/js/ace-builds ]; then
  git clone https://github.com/ajaxorg/ace-builds assets/js/ace-builds
else
  echo "ace-builds already exists. delete the assets/js/ace-builds directory to reinstall"
fi

# Make common packages directory
mkdir -p packages

echo ""
echo "Dependencies"
echo "============"

bundle install
npm install

# Install targets you want
./scripts/riscv64/install.sh
