// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from './event_component.js';
import { RegisterState }  from './register_state.js';

/**
 * Represents a simulator of some hardware platform.
 *
 * Simulators need to follow some strict logic to work with the rest of the
 * system. They are state driven. They start in the `STATE_UNINITIALIZED` state
 * and switch to `STATE_READY` when they are runnable. While they are running
 * they are in the `STATE_RUNNING` state and when they have run but are
 * currently paused they are in the `STATE_PAUSED` state. Finally, when the
 * simulation finishes for any reason, the simulator reaches the `STATE_DONE`
 * state.
 *
 * There are several rules dictating the logic of how states can transition
 * from one to another. The simulator starts in `STATE_UNINITIALIZED` and can
 * only go to `STATE_READY` from there. The simulator can never go back to the
 * `STATE_UNINITIALIZED` state. All simulators are expected to be able to reset
 * and go back to that same initial `STATE_READY` state at any point.
 *
 * Once in `STATE_DONE`, the machine cannot do anything until the `reset`
 * function puts it back into `STATE_READY`.
 *
 * Therefore, a possible state transition would be: `STATE_UNINITIALIZED` to
 * `STATE_READY` to `STATE_RUNNING` to `STATE_PAUSED` to `STATE_RUNNING` to
 * `STATE_DONE` to `STATE_READY` (`reset()`) back to `STATE_RUNNING` to
 * `STATE_DONE` again.
 */
export class Simulator extends EventComponent {
    /**
     * Constructs a new Simulator.
     *
     * @param {string} basepath - The base path for any URL.
     * @param {Object} settings - The initial configuration of the simulator.
     * @param {Target} target - The target architecture of this simulator.
     * @param {LabelListing} labelListing - The label listing.
     */
    constructor(console, basepath, settings, target, labelListing) {
        super();

        // Retain console
        this.__console = console;

        // Retain basepath
        this.__basepath = basepath;

        // Retain the label listing component
        this.__labelListing = labelListing;

        // The initial binaries to simulate
        this.__bootBinary = null;
        this.__runBinary = null;

        // The list of files to mount
        this.__filesLookup = {};
        this.__files = [];

        // The files we have simply seen
        this.__seen = [];

        // The simulation state
        this.__state = Simulator.STATE_UNINITIALIZED;

        // Retain settings (allowing passed settings to override options)
        this.__settings = {
            ...this.options,
            ...settings
        };

        // Retain the target
        this.__target = target;

        // Construct the register state from the register description
        this.__registerState = new RegisterState(this.settings.registers || {});
    }

    /**
     * Returns the name of the target this simulates.
     */
    get target() {
        return this.__target.name;
    }

    /**
     * Returns the label listing instance.
     */
    get labelListing() {
        return this.__labelListing;
    }

    /**
     * Returns the console instance.
     */
    get console() {
        return this.__console;
    }

    /**
     * Retrieves the options.
     */
    get options() {
        return this.constructor.options;
    }

    /**
     * Returns information about the capabilities of the simulator.
     */
    static get options() {
        return {};
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this.__basepath;
    }

    /**
     * Returns the configuration for the simulation.
     */
    get settings() {
        return this.__settings;
    }

    /**
     * Returns information about the simulation.
     */
    get info() {
        throw new Error("target simulator unimplemented");
    }

    /**
     * Retrieves a set of blobs for uploaded files in '/input'.
     */
    get files() {
        return this.__files;
    }

    /**
     * Purges all uploaded files to the '/input' path.
     */
    clearFiles() {
        this.__filesLookup = {};
        this.__files = [];
    }

    /**
     * Purges all files in the working directory.
     */
    clearWorking() {
        this.__working = [];
    }

    /**
     * Adds a read-only text file to the file-system under '/input'.
     *
     * @param {string} name - The file name to add to the '/input' directory.
     * @param {(text|Blob)} source - The source text or Blob.
     */
    upload(name, source) {
        let fileData = source;
        if (!(source instanceof Blob)) {
            // Ensure it has a newline at the end
            source = source + "\n";

            // Create a blob
            fileData = new Blob([source], {'type': 'text/plain'});
        }

        this.__filesLookup[name] = {
            name: name,
            data: fileData
        };
        this.__files.push(this.__filesLookup[name]);
    }

    /**
     * Retrieves the file data for the given file uploaded via `upload`.
     */
    download(name) {
        return this.__filesLookup[name];
    }

    /**
     * Simply notifies this process that a file exists with a particular name.
     */
    notify(name) {
        this.__seen.push(name);
    }

    /**
     * Returns the files that were seen by `notify`.
     */
    get seen() {
        return this.__seen;
    }

    /**
     * Retrieves the register state.
     *
     * @returns {RegisterState} The current register state.
     */
    get registers() {
        return this.__registerState;
    }

    /**
     * Sets the register state.
     *
     * The register state is a dictionary in the same shape as the one provided
     * by the `options` information block.
     *
     * Therefore, if the `options` block has:
     *
     * ```
     * {
     *   registers: {
     *     general: {
     *       ax: Register.int64,
     *       bx: Register.int64,
     *       cx: Register.int64,
     *       dx: Register.int64
     *     },
     *     system: {
     *       gs: 64
     *     },
     *     fpu: {
     *       fp0: 64.0
     *     }
     *   }
     * }
     * ```
     *
     * This function would be called with:
     *
     * ```
     * simulator.registers = { general: [4, 1, 6, 7], system: [0] };
     * ```
     *
     * And can also be used as:
     *
     * ```
     * simulator.registers.general.ax = 4;
     * ```
     */
    set registers(value) {
        this.registers.update(value);
    }

    /**
     * Retrieves the breakpoints.
     */
    get breakpoints() {
        return [];
    }

    /**
     * Returns information about system memory.
     */
    get memory() {
        return {};
    }

    /**
     * Returns information about the binary to boot.
     *
     * The simulation initially runs this binary code. This is often a BIOS or
     * kernel or some other similar system program.
     *
     * Some simulations might have their own boot binary that they provide as
     * part of their simulation or a default they might use.
     */
    get bootBinary() {
        return this.__bootBinary;
    }

    /**
     * Sets the boot binary.
     *
     * If done when the simulation has started, this will stop the simulation
     * and force a reset of the simulator.
     */
    set bootBinary(value) {
        if (this.state !== Simulator.STATE_UNINITIALIZED) {
            // We should stop and reset the machine
            this.stop();
        }

        this.__bootBinary = value;
    }

    /**
     * Returns information about the binary to run.
     *
     * The simulation's boot binary will generally determine how to run this.
     * This binary is often a kernel or, if the boot binary is a kernel, a bare
     * metal program.
     */
    get runBinary() {
        return this.__runBinary;
    }

    /**
     * Sets the binary to run.
     */
    set runBinary(value) {
        if (this.state !== Simulator.STATE_UNINITIALIZED) {
            // We should stop and reset the machine
            this.stop();
        }

        this.__runBinary = value;
    }

    /**
     * Whether or not the simulation is running.
     */
    get running() {
        return this.__state === Simulator.STATE_RUNNING;
    }

    /**
     * Whether or not the simulation is paused.
     */
    get paused() {
        return this.__state === Simulator.STATE_PAUSED;
    }

    /**
     * Whether or not the simulation is ready.
     */
    get ready() {
        return this.__state !== Simulator.STATE_UNINITIALIZED &&
            this.__state !== Simulator.STATE_DONE;
    }

    /**
     * Whether or not the simulation has ended.
     */
    get done() {
        return this.__state === Simulator.STATE_DONE;
    }

    /**
     * Returns the current, active program counter.
     */
    get pc() {
    }

    /**
     * Returns the current state of the simulation.
     *
     * These states can be one of the following:
     *
     * - `Simulator.STATE_UNINITIALIZED`: The simulator has not been initialized.
     * - `Simulator.STATE_READY`: The simulator is ready to run.
     * - `Simulator.STATE_RUNNING`: The simulator is running.
     * - `Simulator.STATE_PAUSED`: The simulator is paused.
     * - `Simulator.STATE_DONE`: The simulator has completed its simulation.
     *
     * @returns {string} The state of the simulation.
     */
    get state() {
        return this.__state;
    }

    /**
     * Sets the state of the simulation.
     *
     * These states can be one of the following:
     *
     * - `Simulator.STATE_READY`: The simulator is ready to run.
     * - `Simulator.STATE_RUNNING`: The simulator is now running.
     * - `Simulator.STATE_PAUSED`: The simulator is now paused.
     * - `Simulator.STATE_BREAKPOINT`: The simulator is now paused via breakpoint.
     * - `Simulator.STATE_DEBUGGER`: The simulator is now paused to the debugger.
     * - `Simulator.STATE_DONE`: The simulator has completed its simulation.
     *
     * Any other state value will result in an error being raised.
     *
     * @param {string} value - The state of the simulation.
     */
    set state(value) {
        // Get the proper value for substates
        let reason = "";
        if (value === Simulator.STATE_PAUSED_BREAKPOINT) {
            value = Simulator.STATE_PAUSED;
            reason = "Breakpoint reached.";
        }
        else if (value === Simulator.STATE_PAUSED_DEBUGGER) {
            value = Simulator.STATE_PAUSED;
            reason = "Debugger taking control.";
        }

        // Throw when attempting to set an invalid state
        if ([Simulator.STATE_READY,
             Simulator.STATE_RUNNING,
             Simulator.STATE_PAUSED,
             Simulator.STATE_DONE].indexOf(value) == -1) {
            throw new Error("simulator state is invalid");
        }
        
        // Check if already this state
        if (this.__state === value) {
            // Ignore if so
            return;
        }

        console.log("setting from", this.state, "to", value);

        // Double check consistency of the state transition logic
        if (this.__state == Simulator.STATE_DONE && value !== Simulator.STATE_READY) {
            // Simulators must only go from 'done' to 'ready' (should be via 'reset')
            throw new Error("simulator cannot go from 'done' to anything other than 'ready'");
        }

        // Double check consistency of the state transition logic
        if (this.__state == Simulator.STATE_UNINITIALIZED && value !== Simulator.STATE_READY) {
            // Simulators must only go from 'uninitialized' to 'ready' (should be via 'load')
            throw new Error("simulator cannot go from 'uninitialized' to anything other than 'ready'");
        }

        // Change the internal state
        this.__state = value;

        // Trigger an event when the state changes
        this.trigger('change', reason);
    }

    /**
     * Initializes the simulation to prepare to run.
     *
     * Returns on error or when the simulator state is `STATE_READY`.
     */
    async load() {
        // Does nothing, if unimplemented
    }

    /**
     * Resets the simulation.
     *
     * Returns on error or when the simulator state is `STATE_READY`.
     */
    async reset() {
        throw new Error("target simulator does not support resetting");
    }

    /**
     * Resumes simulation.
     *
     * Returns on error or when the simulator state is `STATE_RUNNING`.
     */
    async resume() {
        throw new Error("target simulator unimplemented");
    }

    /**
     * Runs the simulation.
     *
     * Returns on error or when the simulator state is `STATE_RUNNING`.
     */
    async run() {
        throw new Error("target simulator unimplemented");
    }

    /**
     * Shuts down the simulation.
     *
     * The next time the simulation is resumed, it will be reset first.
     *
     * Returns on error or when the simulator state is `STATE_DONE`.
     */
    async stop() {
        if (this.state === Simulator.STATE_UNINITIALIZED ||
            this.state === Simulator.STATE_DONE) {
            return;
        }

        // Simulate a 'stop' by pausing and going to the DONE state.

        // Pause
        await this.pause();

        // We could explicitly 'power off' the machine, but we will just have
        // pause issue a 'done' state change from 'paused'. Simulators should
        // override this behavior and do a proper power down.
        this.state = Simulator.STATE_DONE;
    }

    /**
     * Pauses the simulation.
     *
     * Returns on error or when the simulator state is `STATE_PAUSED`.
     */
    async pause() {
        throw new Error("target simulator does not support pausing");
    }

    /**
     * Runs just a single step of the simulation.
     */
    step() {
        throw new Error("target simulator does not support stepping");
    }

    /**
     * Reads a value from memory.
     *
     * @param {number} address - The address to read from.
     * @param {number} size - The size of memory to read.
     * @returns {Uint8Array} An array of unsigned 8-bit numbers.
     */
    read(address, size = 32) {
        throw new Error("target simulator does not support reading from memory");
    }

    /**
     * Writes a value to memory at the given address.
     *
     * @param {number} address - The address to read memory at.
     * @param {Uint8Array} data - An array of unsigned 8-bit numbers.
     */
    write(address, data) {
        throw new Error("target simulator does not support writing to memory");
    }

    /**
     * Adds a breakpoint, if supported.
     *
     * @param {string} address - The address to break upon reaching.
     */
    setBreakpoint(address) {
        throw new Error("target simulator does not support breakpoints");
    }

    /**
     * Removes a previously attached breakpoint via its address.
     *
     * @param {string} address - The address of the breakpoint to remove.
     */
    clearBreakpoint(address) {
        throw new Error("target simulator does not support breakpoints");
    }

    /**
     * Prints out the CPU state to the console.
     *
     * @param {function} log - The logging function to use.
     */
    dump(log) {
        throw new Error("target simulator unimplemented");
    }

    /**
     * Registers an MMIO device, if possible.
     */
    registerMMIO(device) {
        throw new Error("target simulator unimplemented");
    }
}

/**
 * A unit specifier for 'bytes'.
 */
Simulator.B = "B";

/**
 * A unit specifier for 'kibibytes'.
 */
Simulator.KB = "KiB";

/**
 * A unit specifier for 'mebibytes'.
 */
Simulator.MB = "MiB";

/**
 * Represents the simulator state as 'uninitialized'.
 */
Simulator.STATE_UNINITIALIZED = "uninitialized";

/**
 * Represents the simulator state as 'running'.
 */
Simulator.STATE_RUNNING = "running";

/**
 * Represents the simulator state as 'paused'.
 */
Simulator.STATE_PAUSED = "paused";

/**
 * Represents the simulator state as 'paused' via a breakpoint.
 */
Simulator.STATE_PAUSED_BREAKPOINT = "breakpoint";

/**
 * Represents the simulator state as 'paused' to defer to the debugger.
 */
Simulator.STATE_PAUSED_DEBUGGER = "debugger";

/**
 * Represents the simulator state as 'ready'.
 *
 * The simulator is 'ready' when the simulator is loaded but has not yet run.
 */
Simulator.STATE_READY = "ready";

/**
 * Represents the simulator state as 'done'.
 *
 * The simulator is 'done' when the simulation has ended.
 */
Simulator.STATE_DONE = "done";
