// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Dialog } from '../ui/dialog';
import { FileListing } from '../ui/file_listing';
import { FileSystem } from '../file_system';

/**
 * Represents the file copy dialog.
 */
export class CopyDialog extends Dialog {
    constructor(fileInfo) {
        super('dialog-copy-file');

        this.open();

        this._source = fileInfo;

        this._fileList = new FileList(this.dialog);

        this._fileList.loadRoot().then( () => {
            this._fileList.revealPath("/").then( () => {
            })
        });

        this._cancelButton = this.dialog.querySelector("#button-cancel-copy");
        this._copyButton = this.dialog.querySelector("#button-complete-copy");
        this._copyButton.setAttribute('disabled', '');

        // Bind the specific events for the dialog
        this.bindEvents();
    }

    /**
     * Closes the dialog.
     */
    close() {
        this._fileList.unselect();
        this._fileList.clear();
        super.close();
    }

    /**
     * Performs the copy of the file to the selected directory.
     */
    async copy() {
        this._copyButton.setAttribute('disabled', '');
        await this.trigger('copy', this._selected);
        this.close();
    }

    /**
     * Binds events to the dialog to handle the file copy and close buttons.
     *
     * This is called internally when the dialog is created.
     */
    bindEvents() {
        // Bail if this was called before
        if (this.dialog.classList.contains("bound")) {
            return;
        }

        // Do not allow the events to be bound twice
        this.dialog.classList.add("bound");

        this._cancelButton.addEventListener("click", this.close.bind(this));
        this._copyButton.addEventListener("click", this.copy.bind(this));

        // Bind to the change event of the file list
        // This fires when something new is selected
        this._fileList.on('change', (item) => {
            let info = this._fileList.infoFor(item);
            this._selected = info;
            this._copyButton.removeAttribute('disabled');
        });
    }
}
