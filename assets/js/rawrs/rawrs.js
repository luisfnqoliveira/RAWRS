// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Tabs }            from './ui/tabs';
import { Editor }          from './ui/editor';
import { Panels }          from './ui/panels';
import { Toolbar }         from './ui/toolbar';
import { Console }         from './ui/console';
import { FileListing }     from './ui/file_listing';
import { CodeListing }     from './ui/code_listing';
import { MemoryListing }   from './ui/memory_listing';
import { LabelListing }    from './ui/label_listing';
import { RegisterListing } from './ui/register_listing';
import { TargetSelector }  from './ui/target_selector';
import { Separator }       from './ui/separator';

import { Util }            from './util';
import { Processor }       from './processor';
import { Locale }          from './locale';
import { Simulator }       from './simulator';

export class RAWRS {
    // Load the application
    constructor() {
        // Set rootpath
        document.body.setAttribute('data-rootpath', this.rootpath);

        // Bind global keybinds
        document.body.addEventListener('keydown', (ev) => {
            if (ev.code == "F1" && ev.ctrlKey) {
                // Switch to edit tab
                this.tabs.select("assemble-panel");
            }
            else if (ev.code == "F2" && ev.ctrlKey) {
                // Switch to run tab
                this.tabs.select("run-panel");
            }
            else if (ev.code == "F3" && ev.ctrlKey) {
                // Switch to guidance tab
                this.tabs.select("guidance-panel");
            }
        });

        // Load all separators
        let separators = Array.from(
            document.querySelectorAll(".separator")
        ).map( (el) => {
            return new Separator(el);
        });

        // Ensure we go to the indicated anchor
        if (window.location.hash) {
            let item = document.querySelector(window.location.hash);
            if (item) {
                item.scrollIntoView();
            }
        }

        // Create the interface elements
        //this._debugConsole = new Console("#gdb_container", 27, 80, 15);
        this._console = new Console(document.body);
        this._panels = new Panels(document.body);
        this._toolbar  = new Toolbar(document.body);
        this._codeListing = new CodeListing(document.body);
        this._memoryListing = new MemoryListing(document.body);
        this._labelListing = new LabelListing(document.body);
        this._targetSelector = new TargetSelector(this.basepath, document.body);
        this._registerListing = new RegisterListing(document.body);
        this._fileListing = new FileListing(this.basepath, document.body, this.targetSelector.target);

        // We need to populate the file listing with a new target upon change
        this.targetSelector.on('change', async (target) => {
            // Load the target, if it needs to be
            await Util.loadScript(`${this.basepath}js/compiled-${target}.js`);

            // Get the target
            this.target = window.RAWRSTargetFor(target);
        });

        // Pull in the target list and then set the target from last load
        this.targetSelector.populate().then( () => {
            this.targetSelector.target = this.fileListing.target;
        });

        // Ensure we localize any new panels
        this.panels.on('dom-update', () => {
            if (this.locale) {
                this.locale.update(this.panels.element);
            }
        });

        // The active annotations of other files
        this._annotations = {};

        // Load the tabs
        Tabs.load();
        this._tabs = Tabs.load(document.querySelector('#main-tabs'));
        this.tabs.on('change.blur-editor', (button) => {
            if (button.getAttribute('aria-controls') !== 'assemble-panel') {
                // Unfocus the editor
                this.editor.blur();
            }
        });

        // Create an instance of the code editor for the '#editor' textarea
        this._editor = new Editor("editor");

        // Sets up a save timer for the editor
        var saveTimer = null;
        this.editor.on('change', () => {
            if (saveTimer) {
                window.clearTimeout(saveTimer);
            }

            saveTimer = window.setTimeout( () => {
                var data = this.editor.value;
                this.fileListing.save(data);
            }, 500);
        });

        // When a file item has changed
        this.fileListing.on('change', (item) => {
            let info = this.fileListing.infoFor(item);
        });

        // When the file listing suggests somebody wants to open a file
        this.fileListing.on('load', (info) => {
            // Load the file into the editor
            this.editor.load(info.data, info.path);

            // Focus on the editor if the edit tab is open
            if (this.tabs.selected.getAttribute('aria-controls') === 'assemble-panel') {
                this.editor.focus();
            }

            // Load any annotations for this file
            if (info.path in this._annotations) {
                this.editor.annotations = this._annotations[info.path];
            }
        });

        // When a breakpoint is set
        this.codeListing.on("breakpoint-set", (address) => {
            // Tell the simulator
            if (this.simulator) {
                this.simulator.setBreakpoint(address);
            }

            // Tell the debugger
            if (this.debugger) {
                this.debugger.setBreakpoint(address);
            }
        });

        // When a breakpoint is cleared
        this.codeListing.on("breakpoint-clear", (address) => {
            // Tell the simulator
            if (this.simulator) {
                this.simulator.clearBreakpoint(address);
            }

            // Tell the debugger
            if (this.debugger) {
                this.debugger.clearBreakpoint(address);
            }
        });

        // Toolbar events
        this.toolbar.on('click', async (button) => {
            switch (button.getAttribute("id")) {
                case "assemble":
                    this.toolbar.setStatus("run", "disabled");
                    this.toolbar.setStatus("assemble", "active");
                    await this.assemble();
                    break;
                case "run":
                    this.toolbar.setStatus("step", "disabled");
                    this.toolbar.setStatus("run", "active");
                    if (this.toolbar.getStatus("assemble") === "success") {
                        await this.run();
                    }
                    else {
                        this.toolbar.setStatus("run", "disabled");
                        this.toolbar.setStatus("assemble", "active");
                        await this.assemble();
                        await this.run();
                    }
                    break;
                case "step":
                    if (this.simulator && this.simulator.running) {
                        // Pause, if still running
                        await this.pause();
                    }
                    else {
                        // Otherwise, step
                        await this.step();
                    }
                    break;
                default:
                    // Unknown button
                    break;
            }
        });

        // Load the locale
        // TODO: handle the english fallback
        this.locale = new Locale(this.basepath);
    }

    /**
     * Retrieves the current locale data.
     */
    get locale() {
        return this._locale;
    }

    /**
     * Updates the locale and re-translates the application.
     */
    set locale(value) {
        this._locale = value;

        // When the locale loads, translate the document based on our locale
        this.locale.update(document.body);
    }

    /**
     * Retrieves the current target.
     */
    get target() {
        return this._target;
    }

    /**
     * Updates the current Target.
     */
    set target(value) {
        this._target = value;

        // Update the file listing
        this.fileListing.target = this.target.name;

        // Update the editor
        this.editor.target = this.target;

        // Reload register listing
        this.registerListing.description = this.target.registers;

        // Update panels
        this.panels.clear();

        // Create the simulator instance for this target
        if (this.target.simulator) {
            let settings = {
                registers: this.target.registers || {},
                outputs: this.target.simulator.options.outputs || []
            };

            // Create output tabs
            (settings.outputs || []).forEach( (output, i) => {
                settings.outputs[i] = new output.panel(this.basepath, output);
                this.panels.add(settings.outputs[i]);
            });

            this._simulator = new this.target.simulator(this.console, this.basepath, settings, this.target, this.labelListing);
        }
        else {
            this._simulator = null;
        }

        if (this.target.debugger) {
            // Create the simulator instance for this target
            let debuggerSettings = {
                registers: this.target.registers || {},
                outputs: this.target.debugger.options.outputs || []
            };

            // Create output tabs
            (debuggerSettings.outputs || []).forEach( (output, i) => {
                // Debugger tab is always labeled as such
                if (i == 0) {
                    output.name = "run.panels.debugger";
                }
                debuggerSettings.outputs[i] = new output.panel(this.basepath, output);
                this.panels.add(debuggerSettings.outputs[i]);
            });

            // Pass the files to a new debugger (if any; when it is ready)
            this._debugger = new this.target.debugger(this.basepath, debuggerSettings, this.simulator, this.target);

            this.debugger.on("step", () => {
                if (!this.debugger.simulator.done) {
                    this.step();
                }
            });

            this.debugger.on("continue", () => {
                if (!this.debugger.simulator.done) {
                    this.run();
                }
            });

            this.debugger.on("breakpoint-set", (address) => {
                this.codeListing.check(address);
                if (this.simulator) {
                    this.simulator.setBreakpoint(address);
                }
            });

            this.debugger.on("breakpoint-clear", (address) => {
                this.codeListing.uncheck(address);
                if (this.simulator) {
                    this.simulator.clearBreakpoint(address);
                }
            });
        }
        else {
            this._debugger = null;
        }
    }

    /**
     * Returns the active debugger, if any.
     */
    get debugger() {
        return this._debugger;
    }

    /**
     * Returns the runtime panel interface.
     */
    get panels() {
        return this._panels;
    }

    /**
     * Returns the output console.
     */
    get console() {
        return this._console;
    }

    /**
     * Returns the CodeListing element of the interface.
     */
    get codeListing() {
        return this._codeListing;
    }

    /**
     * Returns the LabelListing element of the interface.
     */
    get labelListing() {
        return this._labelListing;
    }

    /**
     * Returns the TargetSelector element of the interface.
     */
    get targetSelector() {
        return this._targetSelector;
    }

    /**
     * Returns the RegisterListing element of the interface.
     */
    get registerListing() {
        return this._registerListing;
    }

    /**
     * Returns the MemoryListing element of the interface.
     */
    get memoryListing() {
        return this._memoryListing;
    }

    /**
     * Returns the Tabs element of the main tabs on the interface.
     */
    get tabs() {
        return this._tabs;
    }

    /**
     * Returns the Editor element of the interface.
     */
    get editor() {
        return this._editor;
    }

    /**
     * Returns the FileListing element of the interface.
     */
    get fileListing() {
        return this._fileListing;
    }

    /**
     * Returns the Toolbar element of the interface.
     */
    get toolbar() {
        return this._toolbar;
    }

    /**
     * Returns the base URL path for any resources.
     *
     * This is useful when the application is hosted off a subroute or different
     * routes of the static site are accessed upon first load.
     */
    get rootpath() {
        // Determine the rootpath for any relative ajax calls later on
        let path = window.location.pathname;
        path = path.split("/");
        path = path.slice(0, path.length - 1);
        path = path.join("/");
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        if (!path.endsWith("/")) {
            path = path + "/";
        }
        return window.location.origin + path;
    }

    /**
     * Retrieves the base path for any access.
     */
    get basepath() {
        return document.body.getAttribute('data-basepath');
    }

    /**
     * Retrieves the current active Simulator instance.
     */
    get simulator() {
        return this._simulator;
    }

    async run() {
        if (this.simulator.paused) {
            // Resume simulation
            this.toolbar.setStatus("step", "");
            this.toolbar.setStatus("run", "active");

            // Resume
            await this.simulator.resume();
        }
        else {
            // Run from start!
            // Clears the annotations from the editor when running the simulator again
            this.editor.annotations = [];

            // Stop if the simulation is running
            if (this.simulator.running) {
                await this.simulator.stop();

                if (this.simulator.state !== Simulator.STATE_DONE) {
                    throw new Error("simulator not done after stop");
                }
            }

            // Reset if the simulation is in its 'done' state
            if (this.simulator.done) {
                await this.simulator.reset();

                if (this.simulator.state !== Simulator.STATE_READY) {
                    throw new Error("simulator not ready after reset");
                }
            }

            // Start the simulation
            await this.simulator.run();

            // Enable the pause button
            this.toolbar.setStatus("step", "");
        }
    }

    async stop() {
        await this.simulator.stop();

        if (this.simulator.state !== Simulator.STATE_DONE) {
            throw new Error("simulator did not shut down properly");
        }
    }

    async pause() {
        await this.simulator.pause();

        if (this.simulator.state !== Simulator.STATE_PAUSED) {
            throw new Error("simulator failed to pause");
        }

        // Update toolbar
        this.toolbar.setStatus("run", "");
        this.toolbar.setStatus("step", "active");
    }

    async step() {
        // Get the instruction highlighted that we want to run
        var info = this.codeListing.highlightedLine;

        // Unhighlight
        this.codeListing.unhighlight();

        // Have the simulator 'step' that instruction
        await this.simulator.step(info);
    }

    async assemble() {
        // Clear code listing
        this.codeListing.clear();

        // Clear registers
        this.registerListing.clear();

        // Clear memory
        this.memoryListing.clear();

        // Clear labels
        this.labelListing.clear();

        // Clear annotations
        this.editor.annotations = [];
        var annotations = {};

        // Clear error flags on the file listing
        this.fileListing.clearAnnotations();

        // Clear the console
        this.console.clear();

        // TODO: move ebreak knowledge out of the file processor
        this._ebreaks = [];

        // For each file that has changed, assemble it and then link them.
        // That is: Iterate over the directory of the current project and
        // assemble every file that is newer than any cached assembled result.

        // Get the parent project directory of the open file.
        let projectDirectory = null;
        let current = this.fileListing.active;
        while (current) {
            if (current.classList.contains("project")) {
                projectDirectory = current;
                break;
            }
            current = this.fileListing.parentOf(current);
        }

        // Get the list of files for the project
        let projectInfo = this.fileListing.infoFor(projectDirectory);
        let listing = await this.fileListing.list(projectDirectory);

        // Set up disassembler
        let disassembler = null;
        if (this.target.disassembler) {
            disassembler = new this.target.disassembler(this.console, projectInfo.name, this.basepath, this.target);

            disassembler.on('instruction', (instruction) => {
                this.codeListing.add(instruction);

                if (instruction.code.match('ebreak')) {
                    this.codeListing.check(instruction.address);
                    this._ebreaks.push(instruction.address);
                }
            });
        }

        // Set up dumper
        let memoryDumper = null;
        if (this.target.memoryDumper) {
            memoryDumper = new this.target.memoryDumper(this.console, projectInfo.name, this.basepath, this.target);

            memoryDumper.on('update', (row) => {
                this.memoryListing.update(row.address, row.data);
            });
        }

        // Set up label dumper
        let labelDumper = null;
        if (this.target.labelDumper) {
            labelDumper = new this.target.labelDumper(this.console, projectInfo.name, this.basepath, this.target);

            let labelArray = [];
            labelDumper.on('update', (row) => {
                labelArray.push(row);
            });

            labelDumper.on('done', (row) => {
                labelArray.sort((a, b) => parseInt(a.address, 16) - parseInt(b.address, 16));
                labelArray.forEach( (element) => this.labelListing.update(element.label, element.section, element.bind, element.address) );
            });
        }

        let processor = new Processor(this.basepath, this.target, this.fileListing, this.codeListing, this.console, this.simulator, disassembler);
        this._binary = await processor.build();

        // Parse errors
        if (processor.errors) {
            for (let error of processor.errors) {
                let errorPath = projectInfo.path + "/" + error.file;
                await this.fileListing.annotate(errorPath, error.type);
                this._annotations[errorPath] = this._annotations[errorPath] || [];
                this._annotations[errorPath].push(error);

                if (this.fileListing.activePath == errorPath) {
                    // Set annotations for the current editor
                    this.editor.annotations = this._annotations[errorPath];
                }
            }
        }

        // Do we have a binary?
        if (!this._binary) {
            this.toolbar.setStatus("assemble", "failure");

            /*
            for (let error of linker.errors) {
                let errorPath = projectInfo.path + "/" + error.file;
                await this.fileListing.annotate(errorPath, error.type);
                this._annotations[errorPath].push(error);

                if (this.fileListing.activePath == errorPath) {
                    // Set annotations for the current editor
                    this.editor.annotations = this._annotations[errorPath];
                }
            }
            */
            return;
        }

        // Also, disassemble the binary
        if (this.target.disassembler) {
            disassembler.add(this._binary);
            await disassembler.invoke();
        }

        // And attach the static memory dumper
        if (this.target.memoryDumper) {
            memoryDumper.add(this._binary);
            await memoryDumper.invoke();
        }

        // And the label dumper
        if (this.target.labelDumper) {
            labelDumper.add(this._binary);
            await labelDumper.invoke();
        }

        this.toolbar.setStatus("assemble", "success");
        this.toolbar.setStatus("run", "");

        // On success, go to the run tab
        this.tabs.select('run-panel');

        // TODO: Create debugger and attach to simulator
        //this._debugger = new this.target.debugger(this.basepath, this.simulator);

        // Pass along the binary
        this.simulator.runBinary = this._binary;

        // When the simulator ends
        this.simulator.on("change", (reason) => {
            let state = this.simulator.state;
            console.log("seeing a change", state, reason);
            if (state === Simulator.STATE_READY) {
            }
            else if (state === Simulator.STATE_RUNNING) {
                // Update console
                this.console.writeln(this._process, "");
                this.console.writeln(this._process, "Simulation started.");

                // When the simulator is running, unhighlight
                this.codeListing.unhighlight();

                // Tell the debugger the simulation is running
                // TODO: debugger call-in
            }
            else if (state === Simulator.STATE_DONE) {
                // Update console
                this.console.writeln(this._process, "Simulation ended.");
                this.console.done(this._process, false);

                // Update the toolbar buttons
                this.toolbar.setStatus("run", "success");
                this.toolbar.setStatus("step", "disabled");

                // Update the memory view
                // TODO: have the memory listing do that work via the memory interface
                /*
                    let numRows = this.memoryListing.numberOfRows;
                    let addresses = this.memoryListing.addresses;
                    this.memoryListing.clear();
                    for (let i = 0; i < numRows; i++) {
                        let data = this.simulator.readMemory(parseInt(addresses[i], 16), 32);
                        this.memoryListing.update(addresses[i], data);
                    }
                    */

                // Tell debugger to stop
                // TODO: reconnect debugger
                //this.debugger.showDisconnected();
                if (this.debugger) {
                    this.debugger.disconnect();
                }
            }
            else if (state === Simulator.STATE_PAUSED) {
                console.log("paused detected");
                // Update console
                if (reason) {
                    this.console.writeln(this._process, "Simulation paused: " + reason);
                }
                else {
                    this.console.writeln(this._process, "Simulation paused.");
                }

                this.toolbar.setStatus("run", "paused");
                this.toolbar.setStatus("step", "active");

                // Get updated memory
                // TODO: Move to some memory interface
                /*
                let numRows = this.memoryListing.numberOfRows;
                let addresses = this.memoryListing.addresses;
                this.memoryListing.clear();
                for (let i = 0; i < numRows; i++) {
                    let data = this.simulator.readMemory(parseInt(addresses[i], 16), 32);
                    this.memoryListing.update(addresses[i], data);
                }
                */

                // Tell debugger to stop
                // TODO: reconnect debugger
                //this.debugger.invoke("target remote /dev/serial");
                if (this.debugger) {
                    this.debugger.connect();
                }

                // Highlight code line and scroll to it
                // (really this happens because of the prior event)
                this.codeListing.highlight(this.simulator.pc.value.toString(16));
            }
        });

        // Bind an event when the program counter updates
        this.simulator.pc.on('change', () => {
            // Highlight the address of the current instruction
            this.codeListing.highlight(this.simulator.pc.value.toString(16));
        });

        // TODO: do this within the plugin
        let framebufferRefresh = 0;
        /*
        this.simulator.on("framebuffer-refresh", () => {
            // On the first refresh, switch to video
            if (framebufferRefresh == 0) {
                let videoButton = document.querySelector("button[aria-controls=\"video\"]");
                if (videoButton) {
                    videoButton.parentNode.parentNode.querySelectorAll("li.tab").forEach( (tabElement) => {
                        tabElement.classList.remove("active");
                    });
                    videoButton.parentNode.classList.add("active");
                }

                let videoPanel = document.querySelector(".tab-panel#video");
                if (videoPanel) {
                    videoPanel.parentNode.querySelectorAll("li.tab-panel").forEach( (panelElement) => {
                        panelElement.classList.remove("active");
                    });
                    videoPanel.classList.add("active");
                }

            }
            framebufferRefresh++;
        });*/

        // When the simulator wants to warn us
        this.simulator.on("warning", (data) => {
            const warning = data["warning"];
            const reg = data["reg"];
            const pc = this.simulator.pc.toString(16);
            const instructions_table = this.codeListing.element;
            const instructions = instructions_table.querySelectorAll(".address");

            let pc_index;
            for (let [i, element] of instructions.entries()) {
                if (element.classList.contains("address-" + pc)) {
                    pc_index = i;
                    break;
                }
            }

            // Gets the correct line number for pseudo-instructions of arbitrary length
            let count = 0;
            let line_num;
            do {
                line_num = instructions[pc_index - count++].parentNode.querySelector(".row").textContent;
            } while (line_num === "");

            // Adds the warning icon to the editor next to the corresponding line number
            const annotations = this._editor.annotations;
            const line_warning = {
                row: line_num - 1,
                column: 0,
                type: "warning",
                text: warning + " " + reg
            }
            annotations.push(line_warning);
            this._editor.annotations = annotations;

            this.console.writeln(this._process, "Warning: " + warning + " for register " + reg + " at line " + line_num);
        });

        // Wait until the simulator loads
        this._process = this.console.newProcess("Running", this._binary.name);
        await this.simulator.load(this._process);

        if (this.simulator.state !== Simulator.STATE_READY) {
            throw new Error("simulator not ready", this.simulator.state);
        }

        // Load the register listing with the initial values from the simulator
        this.registerListing.update(this.simulator.registers);

        // And then connect the debugger
        if (this.debugger) {
            this.debugger.load().then( () => {
                this.debugger.connect();
            });
        }
    }
}
