// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Builds a processor for the given target.
 *
 * A processor will be able to build a set of files or perform other operations
 * upon a project.
 */
export class Processor {
    constructor(basepath, target, fileListing, codeListing, terminal, simulator, disassembler) {
        // Retain basepath
        this._basepath = basepath;

        // Retain target
        this._target = target;

        // Retain file listing
        this._fileListing = fileListing;

        // Retain code listing
        this._codeListing = codeListing;

        // Retain the terminal
        this._terminal = terminal;

        // Retain simulator
        this._simulator = simulator;

        // Retain disassembler
        this._disassembler = disassembler;

        // The error listing
        this._errors = [];

        // Determine the project we are building by looking at the file listing.
        let projectDirectory = null;
        let current = this.fileListing.active;
        while (current) {
            if (current.classList.contains("project")) {
                projectDirectory = current;
                break;
            }
            current = this.fileListing.parentOf(current);
        }

        // Get the project info
        this._projectInfo = this.fileListing.infoFor(projectDirectory);
    }

    get errors() {
        return this._errors;
    }

    get basepath() {
        return this._basepath;
    }

    get target() {
        return this._target;
    }

    get terminal() {
        return this._terminal;
    }

    get fileListing() {
        return this._fileListing;
    }

    get codeListing() {
        return this._codeListing;
    }

    get simulator() {
        return this._simulator;
    }

    get disassembler() {
        return this._disassembler;
    }

    get projectInfo() {
        return this._projectInfo;
    }

    /**
     * Looks at the given directory listing to determine processes to run.
     */
    async firstPassCollect(path, listing) {
        // For each file in the project, we want handlers
        for (let name in listing) {
            if (name.startsWith(".")) {
                continue;
            }

            let metadata = listing[name];
            if (metadata.type === 'directory') {
                // Recurse
                let subpath = path + '/' + name;
                let item = this.fileListing.itemFor(subpath);
                let sublisting = await this.fileListing.list(item);
                await this.firstPassCollect(subpath, sublisting);
                continue;
            }

            // Determine the handler for this type of file, if any
            let handler = this.target.handlerFor(name);
            if (handler && !this._handlers[handler]) {
                let realized = new handler(this.terminal, this.projectInfo.name, this.basepath, this.target);
                if (!handler.options.aggregate) {
                    this._firstPass[handler] = realized;
                }
                else {
                    this._secondPass[handler] = realized;
                }

                this._handlers[handler] = realized;
            }
        }
    }

    /**
     * Passes files within given directory to known processes, if desired.
     */
    async firstPassNotify(path, listing) {
        // For each file in the project, we want to upload files to handlers
        for (let name in listing) {
            if (name.startsWith(".")) {
                continue;
            }

            // Get its name
            let metadata = listing[name];
            let fullpath = path + "/" + name;
            let relativePath = fullpath.substring(this.projectInfo.path.length + 1);
            let subItem = this.fileListing.itemFor(fullpath);
            let text = null;
            let blob = null;

            if (metadata.type === 'directory') {
                // Recurse
                let subpath = path + '/' + name;
                await this.fileListing.revealPath(subpath);
                let item = this.fileListing.itemFor(subpath);
                let sublisting = await this.fileListing.list(item);
                await this.firstPassNotify(subpath, sublisting);
                continue;
            }

            // We pass along some source files to the code listing in
            // preparation of the disassembly.
            if (this.disassembler && this.disassembler.options.listing) {
                if (relativePath.match(this.disassembler.options.listing)) {
                    if (!text) {
                        text = await this.fileListing.dataFor(subItem);
                    }
                    this.codeListing.addSource(relativePath, text);
                }
            }

            // We might upload this file to the simulator
            if (this.simulator && this.simulator.options.upload) {
                if (relativePath.match(this.simulator.options.upload)) {
                    if (!blob) {
                        blob = await this.fileListing.blobFor(subItem);
                    }
                    this.simulator.upload(relativePath, blob);
                }
            }

            for (let handler in this._firstPass) {
                handler = this._firstPass[handler];

                if (handler.options.upload) {
                    // Check to see if we need to upload a file
                    if (relativePath.match(handler.options.upload)) {
                        if (!text) {
                            text = await this.fileListing.dataFor(subItem);
                        }
                        handler.upload(relativePath, text);
                    }
                }

                if (handler.options.notify) {
                    if (relativePath.match(handler.options.notify)) {
                        handler.notify(relativePath);
                    }
                }
            }
        }
    }

    /**
     * Runs first pass processes with the files within the given path.
     */
    async firstPassPerform(path, listing) {
        // Perform first pass for all that need it
        for (let name in listing) {
            if (name.startsWith(".")) {
                continue;
            }

            let handler = this.target.handlerFor(name);
            if (!handler) {
                continue;
            }
            handler = this._firstPass[handler];

            // Get its name
            let metadata = listing[name];
            let fullpath = path + "/" + name;
            let relativePath = fullpath.substring(this.projectInfo.path.length + 1);
            let subItem = this.fileListing.itemFor(fullpath);
            let text = null;

            if (metadata.type === 'directory') {
                // Recurse
                let subpath = path + '/' + name;
                await this.fileListing.revealPath(subpath);
                let item = this.fileListing.itemFor(subpath);
                let sublisting = await this.fileListing.list(item);
                await this.firstPassPerform(subpath, sublisting);
                continue;
            }

            let perform = true;
            let cacheName = null;
            let cachePath = null;
            let result = null;
            if (handler.options.cache) {
                // Ah, we cache the output, so lets see if this was cached
                cacheName = handler.options.cache(name);
                cachePath = path + "/" + "." + cacheName;

                // Look up any existing file by the cached name
                let cacheMetadata = listing["." + cacheName] || {};

                // Unless a cached file already exists that is newer than
                // the source file in question, we perform.
                if (cacheMetadata.modified &&
                    cacheMetadata.modified > metadata.modified) {

                    // We don't perform... we just form the result from the cache
                    perform = false;
                    result = {
                        name: cacheName,
                        path: cachePath,
                        data: await this.fileListing._storage.load(cachePath, cacheMetadata.token)
                    };
                }
            }

            if (perform) {
                // Perform the action and, potentially, store back result
                handler.add(handler.download(name));
                try {
                    handler.newProcess(handler.description, name);
                    result = await handler.invoke();
                }
                catch (e) {
                    // Errored
                    this.terminal.fail(handler.process);
                    throw e;
                }

                // Reset handler
                handler.clearWorking();

                // Check for errors
                if (handler.errors.length > 0) {
                    // An error occurred
                    this._errors = this._errors.concat(handler.errors);

                    // TODO: handle this more gracefully in normal control flow
                    return null;
                }

                if (result && result.data && cachePath && handler && handler.options && handler.options.cache) {
                    // Store the object as a hidden file.
                    this.fileListing.save(result.data, cachePath);
                }
            }

            // Determine the handler for the result, if any
            if (result) {
                handler = this.target.handlerFor(result.name);
                if (!this._handlers[handler]) {
                    let realized = new handler(this.terminal, this.projectInfo.name, this.basepath, this.target);
                    if (!handler.options.aggregate) {
                        // TODO: Hmm... this is a first pass handler introduced
                        // during the first pass. It might need to see previous
                        // files if we allow this.
                        this._firstPass[handler] = realized;
                    }
                    else {
                        this._secondPass[handler] = realized;
                    }

                    this._handlers[handler] = realized;
                }

                if (this._secondPass[handler]) {
                    this._secondPass[handler].add(result);
                }
            }
        }

        return true;
    }

    /**
     * Passes files within given directory to known second pass processes.
     */
    async secondPassNotify(path, listing) {
        // Notify/upload for second pass
        for (let name in listing) {
            if (name.startsWith(".")) {
                continue;
            }

            // Get its name
            let metadata = listing[name];
            let fullpath = path + "/" + name;
            let relativePath = fullpath.substring(this.projectInfo.path.length + 1);
            let subItem = this.fileListing.itemFor(fullpath);
            let text = null;

            if (metadata.type === 'directory') {
                // Recurse
                let subpath = path + '/' + name;
                await this.fileListing.revealPath(subpath);
                let item = this.fileListing.itemFor(subpath);
                let sublisting = await this.fileListing.list(item);
                await this.secondPassNotify(subpath, sublisting);
                continue;
            }

            for (let handler in this._secondPass) {
                handler = this._secondPass[handler];

                if (handler.options.upload) {
                    // Check to see if we need to upload a file
                    if (relativePath.match(handler.options.upload)) {
                        if (!text) {
                            text = await this.fileListing.dataFor(subItem);
                        }
                        handler.upload(relativePath, text);
                    }
                }

                if (handler.options.notify) {
                    if (relativePath.match(handler.options.notify)) {
                        handler.notify(relativePath);
                    }
                }
            }
        }
    }

    /**
     * Runs second pass processes with the files within the given path.
     */
    async secondPassPerform(path, listing) {
        let result = null;

        // Perform second pass (only with files generated by first pass)
        for (let handler in this._secondPass) {
            handler = this._secondPass[handler];

            // Perform
            try {
                handler.newProcess(handler.description, handler.project);
                result = await handler.invoke();
            }
            catch (e) {
                // We failed our second pass
                console.log("error", e);
                return null;
            }

            // TODO: handle errors for this second stage handler
            // Check for errors
            if (handler.errors.length > 0) {
                // An error occurred
                this._errors = this._errors.concat(handler.errors);

                // TODO: handle this more gracefully in normal control flow
                return null;
            }

            return result;
        }
    }

    /**
     * Builds a project from the given file.
     */
    async build() {
        // Keep track of what runs when
        this._handlers = {};
        this._firstPass = {};
        this._secondPass = {};

        // Clear errors
        this._errors = [];

        // Get directory listing
        await this.fileListing.revealPath(this.projectInfo.path);
        let item = this.fileListing.itemFor(this.projectInfo.path);
        let listing = await this.fileListing.list(item);

        // First pass does transformative work (compiling)
        await this.firstPassCollect(this.projectInfo.path, listing);
        await this.firstPassNotify(this.projectInfo.path, listing);
        let result = await this.firstPassPerform(this.projectInfo.path, listing);

        // Second pass does aggregate work (linking)
        if (result) {
            await this.secondPassNotify(this.projectInfo.path, listing);
            return await this.secondPassPerform(this.projectInfo.path, listing);
        }
    }
}
