// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

export class Util {
    /**
     * This function retrieves the query value from the given url.
     *
     * @param {string} name The query key to look for.
     * @param {string} [url] The URL to parse. If not given, then it will use
     *                       the current location.
     * @returns {string} If found, the value for the given key is given.
     */
    static getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * Hashes the given string.
     */
    static hash(str) {
        return btoa(str).replaceAll('=', '');
    }

    /*
     * Internal function to convert various JS arrays into a Uint8Array.
     */
    static toU8(data) {
        if (typeof data === 'string' || data instanceof String) {
            data = data.split("").map( (c) => c.charCodeAt(0) );
        }

        if (Array.isArray(data) || data instanceof ArrayBuffer) {
            data = new Uint8Array(data);
        }
        else if (!data) {
            // `null` for empty files.
            data = new Uint8Array(0);
        }
        else if (!(data instanceof Uint8Array)) {
            // Avoid unnecessary copying.
            data = new Uint8Array(data.buffer);
        }
        return data;
    }

    /**
     * Loads the script at the given URL and resolves when is loaded.
     *
     * @param {string} url - The URL of the JavaScript to load.
     */
    static async loadScript(url) {
        if (!Util.loadedScripts[url]) {
            Util.loadedScripts[url] = new Promise( (resolve, reject) => {
                // Add a <script> to the head of the document
                var head = document.head;
                var script = document.createElement('script');
                script.src = url;

                // Pass 'resolve' as a our event handler for the various state
                // change event handler options for different browsers.
                let blah = () => { resolve(); };
                script.onreadystatechange = blah;
                script.onload = blah;

                // Callback for errors
                script.onerror = reject;

                // Append to the <head> and it will now download
                head.appendChild(script);
            });
        }

        return Util.loadedScripts[url];
    }

    /**
     * This will create a new element from a template.
     *
     * This assumes the template has exactly one root child, which will be
     * instantiated and returned.
     *
     * @param {HTMLElement} template - The template element.
     *
     * @returns {HTMLElement} The new element.
     */
    static createElementFromTemplate(template) {
        let ret = null;
        if ('content' in template) {
            ret = document.importNode(template.content, true);
            ret = ret.children[0];
        }
        else {
            ret = template.children[0].cloneNode(true);
        }
        return ret;
    }
}

Util.loadedScripts = {};
