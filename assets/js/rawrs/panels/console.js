// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Panel } from '../panel.js';

/**
 * This represents a terminal.
 */
export class Console extends Panel {
    load() {
        this.element.classList.add("terminal-container");

        // Get the background color from the element
        let panelStyle = window.getComputedStyle(this.element);
        let backgroundColor = panelStyle.backgroundColor;

        this._term = new window.XTerm({
          theme: {
            fontFamily: 'inconsolata',

            // Force the background color to the appropriate color
            background: backgroundColor
          },
          fontFamily: 'inconsolata',
          cols: this.columns,
          rows: this.rows,
          cursorBlink:      true,     // Whether or not the cursor caret blinks
          cursorStyle:      "block",  // Cursor style: block, underline, or bar
          screenReaderMode: true,     // Enables screen-reader support
          tabStopWidth:     8,        // Default tab stop width (in spaces)
          convertEol:       true,     // Turn any '\n' into '\r\n'
        });

        this._term.open(this.element);
        this._term.resize(this.columns, this.rows);

        this._term.element.addEventListener('keyup', (event) => {
            this.trigger('keyup', event);
        });

        this._term.element.addEventListener('keydown', (event) => {
            this.trigger('keydown', event);
        });

        this._term.onData( (bytes) => {
            if (bytes == '\r') {
                this.trigger('keydown', {
                    repeat: 0,
                    keyCode: 13
                });
            }
            this.trigger('data', bytes);
        });

        this.clear();
    }

    get name() {
        return this.settings.name || "run.panels.terminal";
    }

    get rows() {
        return this.settings.rows;
    }

    get columns() {
        return this.settings.columns;
    }

    clear() {
        this._term.write("\x1b[0;40;37m\x1b[2J\x1b[0;0H");
    }

    write(data) {
        this._term.write(data);
    }

    writeln(data) {
        this._term.write(data + "\n");
    }

    shown() {
        this._term.resize(this.columns, this.rows);
    }
}
