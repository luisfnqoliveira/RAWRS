// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from './event_component';

/**
 * This class represents a register value as part of a simulation.
 */
export class Register extends EventComponent {
    /**
     * Constructs a register with the given name, type, and value.
     *
     * The `type` can be one of the following:
     *
     * - `Register.INT8`: An 8-bit integer.
     * - `Register.INT16`: A 16-bit integer.
     * - `Register.INT32`: A 32-bit integer.
     * - `Register.INT64`: A 64-bit integer.
     * - `Register.DOUBLEIEEE754`: A 64-bit floating point value.
     * - `Register.SINGLEIEEE754`: A 32-bit floating point value.
     *
     * The type is used to interpret the value set to this register and the
     * expected standard value returned when the value of the register is
     * queried.
     *
     * @param {string} name - The name of the register.
     * @param {string} type - The type of the register.
     * @param {number} value - The initial value of the register.
     */
    constructor(name, type, value) {
        super();

        // Retain name and type
        this._name = name;
        this._type = type;

        // Set the initial value
        this.value = value;
    }

    /**
     * Returns the name of the register.
     */
    get name() {
        return this._name;
    }

    /**
     * Returns the type of register, which indicates its reprsentation.
     */
    get type() {
        return this._type;
    }

    /**
     * Returns the current value of the register.
     */
    get value() {
        return this._data;
    }

    /**
     * Sets the value of the register to the provided value.
     *
     * The given value can be a string, a number, or a Register (to copy the
     * value). If the value is a string, it will be coerced to the appropriate
     * type of the register.
     */
    set value(value) {
        // Allow the value to be a Register itself
        if (value instanceof Register) {
            value = value.value;
        }

        // If the value differs, set the value and trigger 'change' event
        if (this.value !== value) {
            console.log("changing to", value);
            this._data = value;
            this.trigger('change', this._data);
        }
    }
}

/**
 * Denotes a 64-bit integer register.
 */
Register.INT64 = 'i64';

/**
 * Denotes a 32-bit integer register.
 */
Register.INT32 = 'i32';

/**
 * Denotes a 16-bit integer register.
 */
Register.INT16 = 'i16';

/**
 * Denotes an 8-bit integer register.
 */
Register.INT8  = 'i8';

/**
 * Denotes an 64-bit floating point register.
 */
Register.DOUBLEIEEE754 = 'f64';

/**
 * Denotes an 32-bit floating point register.
 */
Register.SINGLEIEEE754 = 'f32';
