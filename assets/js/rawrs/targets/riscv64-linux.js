// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Target } from '../target.js';

// Gather processes
import { RISCV64Assembler } from './riscv64/assembler.js';
import { RISCV64Linker } from './riscv64/linker.js';
import { RISCV64CCompiler } from './riscv64/c_compiler.js';
import { RISCV64Disassembler } from './riscv64/disassembler.js';
import { RISCV64MemoryDumper } from './riscv64/memory_dumper.js';
import { RISCV64LabelDumper } from './riscv64/label_dumper.js';

// Our simulator
import { RISCV64Simulator } from './riscv64/simulator.js';

// Out debugger
import { RISCV64Debugger } from './riscv64/debugger.js';

// Needed for register types
import { Register } from '../register.js';

/**
 * Provides a RISC-V 64-bit target.
 */
export class RISCV64LinuxTarget extends Target {
    constructor() {
        super();

        // Register .s files with the assembler
        this.registerHandler(/\.s$/, RISCV64Assembler);

        // Register .c files with the C compiler
        this.registerHandler(/\.c$/, RISCV64CCompiler);

        // Register .o files with the linker
        this.registerHandler(/\.o$/, RISCV64Linker);
    }
    /**
     * Retrieves the name of this target.
     */
    get name() {
        return "riscv64-linux";
    }

    get disassembler() {
        return RISCV64Disassembler;
    }

    get memoryDumper() {
        return RISCV64MemoryDumper;
    }

    get labelDumper() {
        return RISCV64LabelDumper;
    }

    get simulator() {
        return RISCV64Simulator;
    }

    get debugger() {
        return RISCV64Debugger;
    }

    get registers() {
        return {
            general: {
                pc:  Register.INT64,
                ra:  Register.INT64,
                sp:  Register.INT64,
                gp:  Register.INT64,
                tp:  Register.INT64,
                t0:  Register.INT64,
                t1:  Register.INT64,
                t2:  Register.INT64,
                s0:  Register.INT64,
                s1:  Register.INT64,
                a0:  Register.INT64,
                a1:  Register.INT64,
                a2:  Register.INT64,
                a3:  Register.INT64,
                a4:  Register.INT64,
                a5:  Register.INT64,
                a6:  Register.INT64,
                a7:  Register.INT64,
                s2:  Register.INT64,
                s3:  Register.INT64,
                s4:  Register.INT64,
                s5:  Register.INT64,
                s6:  Register.INT64,
                s7:  Register.INT64,
                s8:  Register.INT64,
                s9:  Register.INT64,
                s10: Register.INT64,
                s11: Register.INT64,
                t3:  Register.INT64,
                t4:  Register.INT64,
                t5:  Register.INT64,
                t6:  Register.INT64
            },
            fpu: {
                ft0:  Register.DOUBLEIEEE754,
                ft1:  Register.DOUBLEIEEE754,
                ft2:  Register.DOUBLEIEEE754,
                ft3:  Register.DOUBLEIEEE754,
                ft4:  Register.DOUBLEIEEE754,
                ft5:  Register.DOUBLEIEEE754,
                ft6:  Register.DOUBLEIEEE754,
                ft7:  Register.DOUBLEIEEE754,
                fs0:  Register.DOUBLEIEEE754,
                fs1:  Register.DOUBLEIEEE754,
                fa0:  Register.DOUBLEIEEE754,
                fa1:  Register.DOUBLEIEEE754,
                fa2:  Register.DOUBLEIEEE754,
                fa3:  Register.DOUBLEIEEE754,
                fa4:  Register.DOUBLEIEEE754,
                fa5:  Register.DOUBLEIEEE754,
                fa6:  Register.DOUBLEIEEE754,
                fa7:  Register.DOUBLEIEEE754,
                fs2:  Register.DOUBLEIEEE754,
                fs3:  Register.DOUBLEIEEE754,
                fs4:  Register.DOUBLEIEEE754,
                fs5:  Register.DOUBLEIEEE754,
                fs6:  Register.DOUBLEIEEE754,
                fs7:  Register.DOUBLEIEEE754,
                fs8:  Register.DOUBLEIEEE754,
                fs9:  Register.DOUBLEIEEE754,
                fs10: Register.DOUBLEIEEE754,
                fs11: Register.DOUBLEIEEE754,
                ft8:  Register.DOUBLEIEEE754,
                ft9:  Register.DOUBLEIEEE754,
                ft10: Register.DOUBLEIEEE754,
                ft11: Register.DOUBLEIEEE754
            },
            system: {
                sscratch: Register.INT64,
                sepc:     Register.INT64,
                scause:   Register.INT64,
                stval:    Register.INT64
            }
        };
    }
}

if (window.RAWRSRegisterTarget) {
    window.RAWRSRegisterTarget(RISCV64LinuxTarget);
}
