// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Target } from '../target.js';

import { X86MSDOSAssembler } from './x86-msdos/assembler.js';
import { X86MSDOSLinker } from './x86-msdos/linker.js';
import { X86MSDOSCCompiler } from './x86-msdos/c_compiler.js';
import { X86MSDOSDisassembler } from './x86-msdos/disassembler.js';
import { X86MSDOSMemoryDumper } from './x86-msdos/memory_dumper.js';
import { X86MSDOSLabelDumper } from './x86-msdos/label_dumper.js';

// DOSBox Simulator
import { X86MSDOSSimulator } from './x86-msdos/simulator.js';

// Needed for register types
import { Register } from '../register.js';

export class X86MSDOSTarget extends Target {
    constructor() {
        super();

        // Register .s files with the assembler
        this.registerHandler(/(?:\.s|\.S)$/, X86MSDOSAssembler);

        // Register .c files with the C compiler
        this.registerHandler(/\.c$/, X86MSDOSCCompiler);

        // Register .cc/.cpp files with the C++ compiler
        //this.registerHandler(/(?:\.cc|\.cpp)$/, X86MSDOSCPPCompiler);

        // Register .o files with the linker
        this.registerHandler(/\.o$/, X86MSDOSLinker);
    }

    /**
     * Retrieves the name of this target.
     */
    get name() {
        return "x86-msdos";
    }

    get disassembler() {
        return X86MSDOSDisassembler;
    }

    get memoryDumper() {
        return X86MSDOSMemoryDumper;
    }

    get labelDumper() {
        return X86MSDOSLabelDumper;
    }

    get simulator() {
        return X86MSDOSSimulator;
    }

    get registers() {
        return {
            general: {
                eip:    Register.INT32,
                eax:    Register.INT32,
                ebx:    Register.INT32,
                ecx:    Register.INT32,
                edx:    Register.INT32,
                edi:    Register.INT32,
                esi:    Register.INT32,
                ebp:    Register.INT32,
                esp:    Register.INT32,
                eflags: Register.INT32,
                cs:     Register.INT16,
                ds:     Register.INT16,
                es:     Register.INT16,
                fs:     Register.INT16,
                gs:     Register.INT16,
                ss:     Register.INT16,
            },
            system: {
                cr0: Register.INT32,
                cr1: Register.INT32,
                cr2: Register.INT32,
                cr3: Register.INT32,
                cr4: Register.INT32,
            }
        };
    }
}

if (window.RAWRSRegisterTarget) {
    window.RAWRSRegisterTarget(X86MSDOSTarget);
}
