// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Assembler } from '../../processes/assembler';

export class RISCV64Assembler extends Assembler {
    /**
     * Retrieves the name of this target.
     */
    get target() {
        return "riscv64";
    }

    /**
     * Retrieves the options.
     */
    static get options() {
        return {
            // cache the .o files
            cache: (name) => {
                return name.substring(0, name.length - 2) + ".o"
            },

            // give us ALL .s files
            upload: /\.s$/
        };
    }

    /**
     * Performs the assembly routine.
     *
     * @override
     */
    perform(resolve, reject) {
        let filename = this.working[0].name;

        // Load the web worker for the assembler
        var worker = new Worker(`${this.basepath}js/targets/${this.target}/binutils/riscv64-unknown-elf-as.js`);

        // Get the basename of the file ("hello.s" becomes "hello")
        let basename = filename.match(/([^/]+)[.].+$/)[1]

        // Update the header
        this.updateHeader("Assembling", filename);

        // Create the step
        this.newStep("Writing", basename + ".o");

        // Create the worker and pass along messages
        worker.onmessage = (e) => {
            var msg = e.data;

            switch(msg.type) {
                case "ready":
                    worker.postMessage({
                        type: "run",
                        MEMFS: [],
                        mounts: [{
                            type: "WORKERFS",
                            opts: {
                                blobs: this.files
                            },
                            "mountpoint": "/input"
                        }],
                        arguments: ["/input/" + filename, "-o", basename + ".o", "-g", "-I", "/input"]
                    });
                    break;

                case "stdout":
                    this.writeln(msg.data);
                    break;

                case "stderr":
                    // Ignore some strange errors emscripten reports sometimes
                    if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                        break;
                    }

                    // Remove "/input/" from prececding the message data
                    if (msg.data.startsWith("/input/")) {
                        msg.data = msg.data.substring("/input/".length);
                    }

                    // Check for error statements
                    var matches = RISCV64Assembler.ERROR_REGEX.exec(msg.data);
                    if (matches) {
                        var error = {
                            file: matches[1],
                            row: parseInt(matches[2]) - 1,
                            column: 0,
                            type: 'error',
                            text: matches[3]
                        };
                        this.errors.push(error);
                        this.trigger('error', error);
                    }

                    this.writeln(msg.data);
                    break;

                case "exit":
                    break;

                case "done":
                    if (msg.data.MEMFS[0]) {
                        this.done();
                    }
                    else {
                        this.fail();
                    }

                    this.trigger('done');
                    if (msg.data.MEMFS[0]) {
                        resolve(msg.data.MEMFS[0]);
                    }
                    else {
                        resolve();
                    }
                    worker.terminate();
                    break;
                default:
                    break;
            }
        };
    }
}

RISCV64Assembler.ERROR_REGEX = /^(\S+):(\d+):\s+Error:\s+(.+)$/;
