// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Simulator } from '../../simulator.js';
import { Util } from '../../util.js';

// Gather panels
import { Console } from '../../panels/console.js';
import { Video } from '../../panels/video.js';

/**
 * Represents a TinyEmu simulation of a riscv64 processor.
 */
export class RISCV64Simulator extends Simulator {
    /**
     * Returns information about the capabilities of the simulator.
     */
    static get options() {
        return {
            // Grant us 32MiB of memory by default
            memory: {
                size: 32 * 1024 * 1024,
                unit: Simulator.B
            },

            outputs: [
                {
                    panel: Console,
                    name: 'run.panels.serial',
                    rows: 27,
                    columns: 80,
                    fontSize: 15
                },
                {
                    panel: Video,
                    name: 'run.panels.video',
                    width: 640,
                    height: 480
                }
            ]
        };
    }

    /**
     * Called internally to set up the filesystem.
     */
    _initializeFS(mounts) {
        var FS = this._module.FS;

        // Borrowed a lot from Kagami, here. They are a veritable superhero.
        mounts.forEach( (mount) => {
            if (mount.type === "MEMFS") {
                return;
            }

            var fs = FS.filesystems[mount.type];
            if (!fs) {
                throw new Error("Bad mount type");
            }

            var mountpoint = mount.mountpoint;
            // NOTE(Kagami): Subdirs are not allowed in the paths to simplify
            // things and avoid ".." escapes.
            if (!mountpoint.match(/^\/[^\/]+$/) ||
                  mountpoint === "/." ||
                  mountpoint === "/.." ||
                  mountpoint === "/tmp" ||
                  mountpoint === "/home" ||
                  mountpoint === "/dev" ||
                  mountpoint === "/work") {
                throw new Error("Bad mount point");
            }

            FS.mkdir(mountpoint);
            FS.mount(fs, mount.opts, mountpoint);
        });

        mounts.forEach( (mount) => {
            if (mount.type === "MEMFS") {
                if (mount.file.name.match(/\//)) {
                    throw new Error("Bad file name");
                }
                let mountpoint = mount.mountpoint || "/work";
                if (!FS.analyzePath(mountpoint, true).exists) {
                    FS.mkdir(mountpoint);
                }
                FS.chdir(mountpoint);
                var fd = FS.open(mount.file.name, "w+");
                var data = Util.toU8(mount.file.data);
                FS.write(fd, data, 0, data.length);
                FS.close(fd);

                // chmod +x for anything suggested as an executable
                if (mount.executable) {
                    FS.chmod(mount.file.name, 0o755);
                }
            }
        });

        if (!FS.analyzePath("/work", true).exists) {
            FS.mkdir("/work");
        }
        FS.chdir("/work");
    }

    /**
     * Returns the terminal panel.
     */
    get terminal() {
        return this._terminal;
    }

    /**
     * Returns the video panel.
     */
    get video() {
        return this._video;
    }

    /**
     * Returns the current entrypoint for the running program.
     */
    get entrypoint() {
        return this._entrypoint;
    }

    /**
     * Returns the list of current breakpoints.
     */
    get breakpoints() {
        return this._breakpoints;
    }

    /**
     * Called internally when the simulator starts.
     */
    _start() {
        // Get the Module reference
        var Module = this._module;

        // This will give us function calls into the C API for TinyEmu
        this._console_write1       = Module.cwrap('console_queue_char', null, ['number']);
        this._fs_import_file       = Module.cwrap('fs_import_file', null, ['string', 'number', 'number']);
        this._display_key_event    = Module.cwrap('display_key_event', null, ['number', 'number']);
        this._display_mouse_event  = Module.cwrap('display_mouse_event', null, ['number', 'number', 'number']);
        this._display_wheel_event  = Module.cwrap('display_wheel_event', null, ['number']);
        this._net_write_packet     = Module.cwrap('net_write_packet', null, ['number', 'number']);
        this._net_set_carrier      = Module.cwrap('net_set_carrier', null, ['number']);
        this._vm_start             = Module.cwrap('vm_start', null, ["string", "number", "string", "string", "number", "number", "number", "string"]);
        this._vm_pause             = Module.cwrap('vm_pause', null, []);
        this._vm_resume            = Module.cwrap('vm_resume', null, []);
        this._vm_step              = Module.cwrap('vm_step', null, []);
        this._cpu_get_regs         = Module.cwrap('cpu_get_regs', null, ["number"]);
        this._cpu_set_regs         = Module.cwrap('cpu_set_regs', null, ["number"]);
        this._force_refresh        = Module.cwrap('force_refresh', null, []);
        this._cpu_set_breakpoint   = Module.cwrap('cpu_set_breakpoint', null, ["number"]);
        this._cpu_clear_breakpoint = Module.cwrap('cpu_clear_breakpoint', null, ["number"]);
        this._cpu_get_mem          = Module.cwrap('cpu_get_mem', null, ["number"]);
    }

    /**
     * Called internally when the simulator ends.
     */
    _quit() {
        // Do not quit twice
        if (this.state === Simulator.STATE_DONE) {
            return;
        }

        // Force a refresh of the video
        this.forceRefresh();

        // Update the registers
        this.updateRegisters();

        // Update s10 and s11 registers (if exited on an ecall)
        // SCAUSE will be 0x8 when the last trap as an ecall
        if (this.registers.system.scause.value == 0x8) {
            this.registers.general.pc = this.registers.system.sepc;
            this.registers.general.s10 = this.registers.system.sscratch;
            this.registers.general.s11 = this.registers.system.stval;
        }

        // Set to 'done'
        this.state = Simulator.STATE_DONE;
    }

    /**
     * Returns the value in the PC register.
     *
     * @returns {bigint} The value of the PC register as an integer.
     */
    get pc() {
        return this.registers.general.pc;
    }

    /**
     * Returns an integer array for the values in the registers.
     *
     * @returns {BigUint64Array} An array of unsigned 64-bit numbers.
     */
    updateRegisters() {
        if (!this._loaded) {
            return new window.BigUint64Array(68);
        }

        // The items are PC, followed by registers 1 through 31, floating point registers 0 through 31.
        // Then sscratch, sepc, scause, stval.
        // The 'zero' register is omitted, of course.
        var cbuf = this._module._malloc(68 * 8);
        this._cpu_get_regs(cbuf);

        // Since we have a 64-bit CPU, the buffer is a 64-bit integer array
        let ret = new BigUint64Array(68);
        for (let i = 0; i < ret.length; i++) {
            // Prepare the values in a little-endian uint32 array
            let index = i * 8;
            let values = new Uint32Array([this._module.getValue(cbuf + index + 0, 'i32'),
                                          this._module.getValue(cbuf + index + 4, 'i32')]);

            // Read the little-endian double and assign it to our output array
            ret[i] = (new DataView(values.buffer)).getBigUint64(0, true);

            if (i < 32) {
                this.registers.general[RISCV64Simulator.REGISTER_NAMES[i]].value = ret[i];
            }
            else if (i < 64) {
                this.registers.fpu[RISCV64Simulator.FP_REGISTER_NAMES[i - 32]].value = ret[i];
            }
            else {
                this.registers.system[RISCV64Simulator.SUPERVISOR_REGISTER_NAMES[i - 64]] = ret[i];
            }
        }

        /* Free the buffer. */
        this._module._free(cbuf);
        return ret;
    }

    /**
     * Returns an array of floating point values contained in the floating point
     * co-processor.
     */
    get _fpRegisters() {
        if (!this._loaded) {
            return new Float64Array(32);
        }

        // The items are PC, followed by registers 1 through 31, floating point registers 0 through 31.
        // Then sscratch, sepc, scause, stval.
        // The 'zero' register is omitted, of course.
        var cbuf = this._module._malloc(68 * 8);
        this._cpu_get_regs(cbuf);

        // Since we have a 64-bit CPU, the buffer is a 64-bit integer array
        // We need to pull out floating point values from that array.
        let ret = new Float64Array(32);
        for (let i = 32; i < 64; i++) {
            // Prepare the values in a little-endian uint32 array
            let values = new Uint32Array([this._module.getValue(cbuf+(8*i)+0, 'i32'),
                                          this._module.getValue(cbuf+(8*i)+4, 'i32')]);

            // Read the little-endian double and assign it to our output array
            ret[i - 32] = (new DataView(values.buffer)).getFloat64(0, true);
        }

        /* Free the buffer. */
        this._module._free(cbuf);
        return ret;
    }

    /**
     * Accepts an integer array for the values in the registers.
     * Transfers this array to the TinyEmu simulator
     *
     * @param {BigUint64Array} buf An array of unsigned 64-bit numbers.
     */
    set _registers(buf) {
        // The items are PC, followed by registers 1 through 32.
        // The 'zero' register is omitted, of course.
        var cbuf = this._module._malloc(68 * 8);

        // Get the current values. We will override those we wish.
        this._cpu_get_regs(cbuf);

        // Create a mask for dividing the 64-bit BigInt to two half-words.
        let mask = BigInt("0xffffffff");

        for (let i = 0; i < buf.length; i++) {
            let index = i * 8;
            let lowInt = Number(BigInt.asIntN(32, buf[i] & mask));
            let highInt = Number(BigInt.asIntN(32, (buf[i] >> BigInt(32)) & mask));
            this._module.setValue(cbuf + index, lowInt, 'i32');
            this._module.setValue(cbuf + index + 4, highInt, 'i32');
        }

        this._cpu_set_regs(cbuf);
        this._module._free(cbuf);
        this.trigger('registers-change');
    }

    /**
     * Returns an integer array for a segment of data from memory
     *
     * @param {number} address The address to read memory at
     * @param {number} size The size of memory to read
     * @returns {Uint8Array} An array of unsigned 8-bit numbers.
     */
    read(address, size = 32) {
        let ptr = this._cpu_get_mem(address);
        let mem = new Uint8Array(size);
        for (let i = 0; i < size; i++) {
            mem[i] = this._module.getValue(ptr + i, 'i8');
        }

        return mem;
    }

    /**
     * At the address location, sets memory according to the accepted integer array.
     *
     * @param {number} address The address to read memory at.
     * @param {Uint8Array} data An array of unsigned 8-bit numbers.
     */
    write(address, data) {
        let ptr = this._cpu_get_mem(address);

        for (let i = 0; i < data.length; i++) {
            this._module.setValue(ptr + i, data[i], 'i8');
        }
    }

    /**
     * Add a breakpoint.
     *
     * @param {string} address The address is break upon reaching.
     */
    setBreakpoint(address) {
        if (!this._breakpoints) {
            this._breakpoints = [];
        }

        // Pad address to make them all the same length
        address = address.padStart(16, "0");

        // Call into emulator
        if (this._cpu_set_breakpoint) {
            // Convert 'address' to a pair of uint32s.
            let high = parseInt(address.slice(0, 8), 16);
            let low  = parseInt(address.slice(8), 16);

            this._cpu_set_breakpoint(high, low);
        }

        this._breakpoints.push(address);
    }

    /**
     * Removes a previously attached breakpoint via its address.
     *
     * @param {string} address The address of the breakpoint to remove.
     */
    clearBreakpoint(address) {
        // Convert 'address' to a pair of uint32s.
        address = address.padStart(16, "0");
        let high = parseInt(address.slice(0, 8), 16);
        let low  = parseInt(address.slice(8), 16);
        // Call into emulator
        this._cpu_clear_breakpoint(high, low);
        this._breakpoints.splice(this._breakpoints.indexOf(address), 1);
    }

    /**
     * Prints out the CPU state to the console.
     */
    dump(log) {
        this.registers.categories.forEach( (category) => {
            category = this.registers[category];

            category.names.forEach( (name) => {
                let reg = category[name];
                var str = reg.value.toString(16);
                str = "0000000000000000".slice(str.length) + str;
                log(name + ": 0x" + str);
            });
        });
    }

    /**
     * Forces a refresh of the display.
     */
    forceRefresh() {
        this._force_refresh();
    }

    /**
     * Initializes the simulation to prepare to run.
     */
    load(process) {
        this._loading = true;
        return new Promise( async (resolve, reject) => {
            this._loading = resolve;
            this._process = process;

            // Do not call twice.
            if (this._initialized) {
                resolve();
                return;
            }
            this._initialized = true;

            // Retain output panels
            this._terminal = this.settings.outputs[0];
            this._video = this.settings.outputs[1];

            [this.video, this.terminal].forEach( (device) => {
                device.on('keydown', (event) => {
                    if (!event.repeat && this.running) {
                        if (this._display_key_event) {
                            this._display_key_event(1, event.keyCode);
                        }
                    }
                });

                device.on('keyup', (event) => {
                    if (this.running) {
                        if (this._display_key_event) {
                            this._display_key_event(0, event.keyCode);
                        }
                    }
                });
            });

            // The terminal can be connected to the tty device
            this.terminal.on('data', (bytes) => {
                for (let i = 0; i < bytes.length; i++) {
                    let b = bytes.charCodeAt(i);
                    this._console_write1(b);
                }
            });

            // Set initial values

            // The address of the next expected breakpoint, which we will clear
            // when we hit it.
            this._clearBreakpoint = false;

            // The simulator has never been loaded
            this._loaded = false;

            // When the simulator has properly loaded and run until the start of
            // the application, this will be 'true'
            this._ready = false;

            // Look for external register changes (from UI / debugger)
            this.registers.on('change', (info) => {
                // Send register data to simulator
                let values = new BigUint64Array(68);

                for (let i = 0; i < values.length; i++) {
                    if (i < 32) {
                        values[i] = BigInt(this.registers.general[RISCV64Simulator.REGISTER_NAMES[i]].value);
                    }
                    else if (i < 64) {
                        values[i] = BigInt(this.registers.fpu[RISCV64Simulator.FP_REGISTER_NAMES[i - 32]].value);
                    }
                    else {
                        values[i] = BigInt(this.registers.system[RISCV64Simulator.SUPERVISOR_REGISTER_NAMES[i - 64]].value);
                    }
                }

                this._registers = values;
            });

            // TinyEMU looks for this:
            window.rawrsVideo = this.video;

            // TinyEMU looks for this:
            window.term = {
                write: (x) => {
                    this.terminal.write(x);
                },
                getSize: (x) => {
                    return [this.terminal.columns, this.terminal.rows];
                }
            };

            // Load the simulator into the environment
            // Afterward, we will have a `window.TinyEmu`
            await Util.loadScript(this.basepath + RISCV64Simulator.VM_URL);

            // Load the default kernel if none was provided instead
            if (!this.bootBinary) {
                let kernelURL = this.basepath + RISCV64Simulator.KERNEL_URL;

                let response = await fetch(kernelURL, { credentials: 'include' });
                let data = await response.arrayBuffer();

                this.bootBinary = {
                    name: "kernel.bin",
                    data: data
                };
            }

            // Load the default kernel if none was provided instead
            if (this.isLinux && !this.kernelBinary) {
                let kernelURL = this.basepath + RISCV64Simulator.LINUX_URL;

                let response = await fetch(kernelURL, { credentials: 'include' });
                let data = await response.arrayBuffer();

                this.kernelBinary = {
                    name: "linux.bin",
                    data: data
                };
            }

            // Load the default bootloader if none was provided instead
            if (this.isLinux && !this.bootloaderBinary) {
                let kernelURL = this.basepath + RISCV64Simulator.BOOTLOADER_URL;

                let response = await fetch(kernelURL, { credentials: 'include' });
                let data = await response.arrayBuffer();

                this.bootloaderBinary = {
                    name: "bbl.bin",
                    data: data
                };
            }

            // Load the root filesystem for Linux
            if (this.isLinux && !this.filesystemBinary) {
                let filesystemURL = this.basepath + RISCV64Simulator.FILESYSTEM_URL;

                let response = await fetch(filesystemURL, { credentials: 'include' });
                let data = await response.arrayBuffer();

                this.filesystemBinary = {
                    name: "root.bin",
                    data: data
                };
            }

            // Load the run binary, if it was provided
            if (!this.runBinary) {
                throw new Error("run binary is required");
            }

            // The initial Module namespace
            this._module = {};

            // Calculate the working memory
            var alloc_size = this.settings.memory.size;
            alloc_size += 32; /* extra space (XXX: reduce it ?) */
            alloc_size = (alloc_size + 15) & -16; /* align to 16 MB */

            // Convert to bytes and set it in the runtime.
            this._module.INITIAL_MEMORY = alloc_size << 20;

            // Ensure that the WASM loader can find the WASM file
            this._module.locateFile = (url) => {
                return this.basepath + 'js/targets/riscv64/tinyemu/' + url;
            };

            // Action to happen on quit.
            this._module.quit = (status, ex) => {
                this._quit();
            };

            // Callback when the simulator hits a breakpoint
            this._module.onBreakpoint = () => {
                this._breakpoint();
            };

            // Callback when the simulator says it is ready
            this._module.onVMReady = () => {
                // The virtual machine is ready to run...

                // Set initial breakpoints
                (this._breakpoints || []).forEach( (address) => {
                    let high = parseInt(address.slice(0, 8), 16);
                    let low  = parseInt(address.slice(8), 16);

                    this._cpu_set_breakpoint(high, low);
                });
            };

            // Callback when the simulator says it is paused
            this._module.onVMPaused = () => {
                // Pull registers (so the state updates upstream)
                this.updateRegisters();

                // Set to 'paused'
                this.state = Simulator.STATE_PAUSED;
            };

            // Callback when the simulator wants to refresh the video
            this._module.onFBRefresh = () => {
                // TODO: pass off to the plugin we would register
                this.trigger("framebuffer-refresh");
            };

            let config = {
                version: 1,
                machine: "riscv64",
                memory_size: 256,
                input_device: "virtio",
                bios: this.bootBinary.name,
                kernel: this.runBinary.name
            };

            if (this.isLinux) {
                config.bios = this.bootloaderBinary.name;
                config.kernel = this.kernelBinary.name;
                config.drive0 = {
                    file: this.filesystemBinary.name
                };
                config.fs0 = {
                    tag: "/dev/root",
                    file: "/input"
                };
                config.eth0 = {
                    driver: "user"
                };
                config.cmdline = "console=hvc0 root=/dev/vda rw init=/sbin/init";
            }
            else {
                // Add the framebuffer
                config.display_device = "simplefb";
            }

            // The callback that fires before TinyEmu has even initialized
            this._module.preRun = () => {
                // Upload the files to TinyEmu's local file-system
                let fs = [];

                // Upload config.cfg
                fs.push({
                    type: "MEMFS",
                    file: {
                        name: "config.cfg",
                        data: JSON.stringify(config)
                    }
                });

                if (this.isLinux) {
                    // Push bootloader
                    fs.push({
                        type: "MEMFS",
                        file: this.bootloaderBinary
                    });

                    // Push Linux kernel
                    fs.push({
                        type: "MEMFS",
                        file: this.kernelBinary
                    });

                    // Push Linux filesystem
                    fs.push({
                        type: "MEMFS",
                        file: this.filesystemBinary
                    });

                    // Push application binary
                    fs.push({
                        type: "MEMFS",
                        file: this.runBinary,
                        mountpoint: "/input",
                        executable: true
                    });
                }
                else {
                    // Push riscv64 basic kernel
                    fs.push({
                        type: "MEMFS",
                        file: this.bootBinary
                    });

                    // Push application binary
                    fs.push({
                        type: "MEMFS",
                        file: this.runBinary
                    });
                }

                this._initializeFS(fs);

                // Clear the terminal
                //this.terminal.clear();

                // Clear video
                //this._video.clear();

                this._start();
            };

            // The callback when the runtime for TinyEmu has initialized
            this._module.onRuntimeInitialized = () => {
                if (this._started) {
                    return;
                }

                this.reset();
            };

            // Ignore the warnings/errors from emscripten's boilerplate
            this._module.printErr = (msg) => {
            };

            // Callback on a VM warning
            this._module.onVMWarn = (warning_code, reg_index) => {
                let warning;
                switch (warning_code) {
                    case 1:
                        warning = "Uninitialized Register";
                        break;
                    case 2:
                        warning = "Unmaintained S-Register After Jump";
                        break;
                    case 3:
                        warning = "Stored Uninitialized Register";
                        break;
                }

                // Gets the register name given an index
                let reg = (reg_index == 0) ? "zero" : RISCV64Simulator.REGISTER_NAMES[reg_index];

                this.trigger("warning", {"warning": warning, "reg": reg});
            };

            // Stop TinyEmu from crashing
            window.update_downloading = function() {};

            // Initialize TinyEmu's Module namespace
            this._module = await window.TinyEmu(this._module);

            this._loaded = true;
        });
    }

    /**
     * Start or resume the simulation.
     */
    async run() {
        this.resume();
    }

    /**
     * Internal function that is the breakpoint callback.
     */
    _breakpoint() {
        // Pull registers
        this.updateRegisters();

        // If this is the initial breakpoint, this was the initialization
        if (this._resetting) {
            console.log("resetting!");
            this.state = Simulator.STATE_READY;
            let callback = this._resetting;
            callback();
        }

        if (this._loading) {
            let callback = this._loading;
            callback();
        }

        // If we have a breakpoint to clear, clear it
        // (This is an internal breakpoint and shouldn't register as a normal
        // breakpoint for the app itself)
        else if (this._clearBreakpoint != false) {
            this.clearBreakpoint(this._clearBreakpoint);
            this._clearBreakpoint = false;
            if (!this._resetting && !this._loading) {
                this.state = Simulator.STATE_PAUSED;
            }
        }
        else {
            if (!this._resetting && !this._loading) {
                this.state = Simulator.STATE_PAUSED_BREAKPOINT;
            }
        }

        this._loading = false;
        this._resetting = false;
    }

    /**
     * Pause the simulation.
     */
    async pause() {
        if (this._ready) {
            this._paused = true;
            this._vm_pause();

            // Set to 'paused'
            this.state = Simulator.STATE_PAUSED;
        }
    }

    /**
     * Resumes the simulation, if it is paused.
     */
    async resume() {
        if (this.state == Simulator.STATE_READY || this.state == Simulator.STATE_PAUSED) {
            // Update state
            this.state = Simulator.STATE_RUNNING;

            // Run!
            this._vm_resume();
        }
    }

    /**
     * Steps a single instruction.
     *
     * @param (Object) info - The information known about the next instruction.
     */
    step(info) {
        if (this.state == Simulator.STATE_READY || this.state == Simulator.STATE_PAUSED) {
            // If the current instruction is an ecall... we want to breakpoint
            // to the following instruction instead of stepping.
            //
            // Since, if we step, we step into the kernel.
            if (info && (info.code == "ecall" || info.code == "scall" || info.code == "mcall")) {
                let address = (window.BigInt("0x" + info.address) + window.BigInt(4)).toString(16);
                this._clearBreakpoint = info.address;
                this.setBreakpoint(address)
                this.resume();
            }
            else {
                this.state = Simulator.STATE_RUNNING;
                this._vm_step();
            }
        }
    }

    get isLinux() {
        return this.target.endsWith("-linux");
    }

    reset() {
        this._resetting = true;
        return new Promise( (resolve, reject) => {
            this._resetting = resolve;

            // Clear output panels
            this.terminal.clear();
            this.video.clear();

            // The default dimensions of the framebuffer
            let width = 1024;
            let height = 768;

            // Our Linux mode does not have a framebuffer... it crashes otherwise
            if (this.isLinux) {
                width = 0;
                height = 0;
            }

            // Restart (initialize) the machine
            this._vm_start("config.cfg",
                           this.settings.memory.size / (1024 * 1024),
                           "",   // cmdline
                           "",   // password
                           width, // width
                           height,  // height
                           0,    // net_state
                           "");  // drive_url

            // We need to run the simulation to run the systems software such that
            // it loads the binary into the virtual address space.
            // Otherwise, debuggers and such that query memory will not work.

            // Discover the entrypoint
            let entrypoint = this.labelListing.retrieve("main");
            if (entrypoint && entrypoint.bind == "global") {
                entrypoint = entrypoint.address;
            }
            else {
                entrypoint = this.labelListing.retrieve("_start");
                if (entrypoint && entrypoint.bind == "global") {
                    entrypoint = entrypoint.address;
                }
                else {
                    entrypoint = "400000";
                }
            }
            this._entrypoint = entrypoint;

            // Set initial breakpoint (which we will clear when we hit it)
            if (!this.isLinux) {
                this.setBreakpoint(entrypoint);
            }

            // Run until the entrypoint is hit
            this._vm_resume();
        });
    }
}

/**
 * The URL of the simulator's JavaScript compiled code to use.
 */
RISCV64Simulator.VM_URL = "js/targets/riscv64/tinyemu/riscvemu64-wasm.js";

/**
 * The URL of the default boot binary (our kernel).
 */
RISCV64Simulator.KERNEL_URL = "static/riscv64/kernel/kernel.bin";

/**
 * The URL of the Linux kernel.
 */
RISCV64Simulator.LINUX_URL = "static/riscv64-linux/boot/Image";

/**
 * The URL of the boot loader.
 */
RISCV64Simulator.BOOTLOADER_URL = "static/riscv64-linux/boot/bbl.bin";

/**
 * The URL of the Linux filesystem/
 */
RISCV64Simulator.FILESYSTEM_URL = "static/riscv64-linux/busybox-root.bin";

/**
 * A mapping of register names to the register vector.
 *
 * The zero register is replaced with the name of the pc register for
 * convenience.
 */
RISCV64Simulator.REGISTER_NAMES = [
    "pc", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "s0", "s1", "a0", "a1",
    "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7",
    "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6",
];

RISCV64Simulator.FP_REGISTER_NAMES = [
    "ft0", "ft1", "ft2", "ft3", "ft4", "ft5", "ft6", "ft7",
    "fs0", "fs1",
    "fa0", "fa1", "fa2", "fa3", "fa4", "fa5", "fa6", "fa7",
    "fs2", "fs3", "fs4", "fs5", "fs6", "fs7", "fs8", "fs9", "fs10", "fs11",
    "ft8", "ft9", "ft10", "ft11"
];

RISCV64Simulator.SUPERVISOR_REGISTER_NAMES = [
    "sscratch", "sepc", "scause", "stval"
];

RISCV64Simulator.ALL_REGISTER_NAMES = Array.prototype.concat(
    RISCV64Simulator.REGISTER_NAMES,
    RISCV64Simulator.FP_REGISTER_NAMES,
    RISCV64Simulator.SUPERVISOR_REGISTER_NAMES
);

/**

        // TODO: move to TinyEmu simulator code

        // TODO: move to TinyEmu simulator code
        window.terminal_warning = {
            warn: (warning_code, reg_index) => {
                let warning;

                switch (warning_code) {
                    case 1:
                        warning = "Uninitialized Register";
                        break;
                }

                // Gets the register name given an index
                let reg;
                if (reg_index == 0) {
                    reg = "zero";
                } else {
                    reg = Simulator.REGISTER_NAMES[reg_index];
                }

                const pc = this.simulator.pc.toString(16);
                const instructions_table = this.codeListing.element;
                const instructions = instructions_table.querySelectorAll(".address");

                let pc_index;
                for (let [i, element] of instructions.entries()) {
                    if (element.classList.contains("address-" + pc)) {
                        pc_index = i;
                        break;
                    }
                }

                let line_num = instructions[pc_index].parentNode.querySelector(".row").textContent;
                // Gets the correct line number for pseudo-instructions of arbitrary length
                let pseudo_count = 0;
                while (line_num === "") {
                    line_num = instructions[pc_index - ++pseudo_count].parentNode.querySelector(".row").textContent;
                }

                const annotations = this.editor.annotations;
                const line_warning = {
                    row: line_num - 1,
                    column: 0,
                    type: "warning",
                    text: warning + " " + reg
                }
                annotations.push(line_warning);
                this.editor.annotations = annotations;

                this.terminal.writeln("Warning: " + warning + " " + reg + " at line " + line_num);
            }
        };

        // TODO: why is this here twice
        window.terminal_warning = {
            warn: (warning_code, reg_index) => {
                let warning;

                switch (warning_code) {
                    case 1:
                        warning = "Uninitialized Register";
                        break;
                }

                // Gets the register name given an index
                let reg;
                if (reg_index == 0) {
                    reg = "zero";
                } else {
                    reg = Simulator.REGISTER_NAMES[reg_index];
                }

                const pc = this.simulator.pc.toString(16);
                const instructions_table = this.codeListing.element;
                const instructions = instructions_table.querySelectorAll(".address");

                let pc_index;
                for (let [i, element] of instructions.entries()) {
                    if (element.classList.contains("address-" + pc)) {
                        pc_index = i;
                        break;
                    }
                }

                let line_num = instructions[pc_index].parentNode.querySelector(".row").textContent;
                // Gets the correct line number for pseudo-instructions
                if (line_num === "") {
                    line_num = instructions[pc_index - 1].parentNode.querySelector(".row").textContent;// Does not get the line number of pseudo-instructions comprised of more than 2 instructions
                }

                const annotations = this._editor.annotations;
                const line_warning = {
                    row: line_num - 1,
                    column: 0,
                    text: warning + " " + reg
                }
                line_warning.type = "warning";
                annotations.push(line_warning);
                this._editor.annotations = annotations;

                terminal.writeln(warning + " " + reg + " at line " + line_num);
            }
        };

        */
