// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Linker } from '../../processes/linker.js';

export class GCC {
    constructor(process) {
        this._process = process;
    }

    get suffix() {
        return "";
    }

    get process() {
        return this._process;
    }

    /**
     * Helper routine to perform an 'exec' callout to a subprogram.
     */
    _exec(filename, argv) {
        return new Promise( (resolve, reject) => {
            // The URL base to use
            let basepath = this.process.basepath;

            // The URL of the worker
            let workerSource = "";

            // Whether or not we include the zipped system root
            let includeSystemRoot = true;

            // Whether or not we know the expected output to give back
            let output = null;

            let basename = (filename.match(/([^/]+)[.].+$/) || [null, filename])[1];

            // The environment variables
            let env = {
                "TMPDIR": "/work",
                /*
                "C_INCLUDE_PATH": `/usr/include:/usr/lib/gcc/${this.process.targetTriple}/10.2.0/include`,
                "CPLUS_INCLUDE_PATH": `/usr/include:/usr/lib/gcc/${this.process.targetTriple}/10.2.0/include`,
                "OBJC_INCLUDE_PATH": `/usr/include:/usr/lib/gcc/${this.process.targetTriple}/10.2.0/include`,
                */
                "LIBRARY_PATH": `/usr/lib:/usr/lib/gcc/${this.process.targetTriple}/10.2.0`
            };

            // Now select argv and specific options by the program we're running
            if (argv[0] == "gcc") {
                this._commands = [];
                workerSource = `${basepath}js/targets/${this.process.target}/gcc/${this.process.targetTriple}-gcc.js`;
            }
            else if (argv[0] == "cc1") {
                // Run C compiler
                workerSource = `${basepath}js/targets/${this.process.target}/gcc/${this.process.targetTriple}-cc1.js`;
                // Generally, it is the last argument
                // We want it to conform to our exacting standards :)
                output = basename + ".s";
                argv[argv.length - 1] = "/work/" + output;
                this.process.newStep("Compiling", output);
            }
            else if (argv[0] == "as") {
                // Run assembler
                workerSource = `${basepath}js/targets/${this.process.target}/binutils/${this.process.targetTriple}-as.js`;
                // Generally, it is the last argument
                // We want it to conform to our exacting standards :)
                argv[argv.length - 1] = "/work/" + basename + ".s";
                output = basename + ".o";
                this.process.newStep("Assembling", output);
            }
            else if (argv[0] == "ld") {
                // Run linker
                workerSource = `${basepath}js/targets/${this.process.target}/binutils/${this.process.targetTriple}-ld.js`;

                // Remove -fno-lto... not needed here
                let index = argv.indexOf("-fno-lto");
                if (index >= 0) {
                    argv.splice(index, 1);
                }
                // It doesn't look for this in the right place for some reason
                index = argv.indexOf("crt1.o");
                if (index >= 0) {
                    argv[index] = "/usr/lib/crt1.o";
                }
                // I dunno... remove -latomic
                index = argv.indexOf("-latomic");
                if (index >= 0) {
                    argv.splice(index, 1);
                }
                // Yeah... add the obvious path for some reason
                argv.push("-L/usr/lib");
                console.log(argv);
                output = this.process.project + this.suffix;
                this.process.newStep("Linking", output);
            }

            var worker = new Worker(workerSource);

            // Local file-system
            let mounts = [
                {
                    type: "WORKERFS",
                    opts: {
                        blobs: this.process.files
                    },
                    "mountpoint": "/input"
                }
            ];

            if (includeSystemRoot) {
                mounts.push({
                    type: "ZIPFS",
                    opts: {
                        blob: this._systemRoot
                    },
                    "mountpoint": "/usr"
                });
            }

            worker.onmessage = async (e) => {
                var msg = e.data;

                switch(msg.type) {
                    case "ready":
                        worker.postMessage({
                            type: "run",
                            thisProgram: (argv[0] === "gcc" ? "/usr/bin/gcc" : `/usr/lib/gcc/${this.process.targetTriple}/10.2.0/${argv[0]}`),
                            MEMFS: this._baseFS.concat(this._workingFS),
                            ENV: env,
                            output: output,
                            mounts: mounts,
                            arguments: argv.slice(1)
                        });
                        break;
                    case "stdout":
                        {
                            if (argv[0] === "gcc") {
                                // Look for our special messages that tell us when
                                // to delegate to subcommands.
                                let matches = GCC.DELEGATE_REGEX.exec(msg.data);
                                if (matches) {
                                    let argv = JSON.parse(matches[1]);
                                    argv[0] = argv[0].substring(argv[0].lastIndexOf('/') + 1);
                                    this._commands.push(argv);
                                }
                                else {
                                    this.process.step.writeln(msg.data);
                                }
                            }
                            else {
                                this.process.step.writeln(msg.data);
                            }
                        }
                        break;
                    case "stderr":
                        {
                            // Remove "/input/" from prececding the message data
                            if (msg.data.startsWith("/input/")) {
                                msg.data = msg.data.substring("/input/".length);
                            }

                            // Ignore the end of line warnings
                            if (msg.data.indexOf("end of file not at end of a line") >= 0) {
                                break;
                            }

                            // Check for error statements
                            let matches = GCC.ERROR_REGEX.exec(msg.data);
                            if (matches) {
                                var error = {
                                    file: matches[1],
                                    row: parseInt(matches[2]) - 1,
                                    column: 0,
                                    type: 'error',
                                    text: matches[3]
                                };
                                this.process.errors.push(error);
                                this.trigger('error', error);
                            }

                            // Ignore some strange errors emscripten reports sometimes
                            if (msg.data.indexOf("warning: unsupported syscall:") >= 0) {
                                break;
                            }

                            // And other spurious messages
                            if (msg.data.indexOf(": Assembler messages:") >= 0) {
                                break;
                            }

                            this.process.step.writeln(msg.data);
                        }
                        break;
                    case "exit":
                        this._lastCode = msg.data;
                        break;
                    case "done":
                        if (argv[0] !== "gcc") {
                            if (msg.data.MEMFS[0]) {
                                this.process.step.done();
                            }
                            else {
                                this.process.step.fail();
                            }
                        }

                        worker.terminate();

                        if (argv[0] === "gcc") {
                            // Set the working file-system
                            this._workingFS = msg.data.MEMFS;

                            // Delegate!
                            
                            for (let argv of this._commands) {
                                try {
                                    this._workingFS = [await this._exec(filename, argv)];
                                }
                                catch {
                                    reject();
                                }
                            }

                            resolve(this._workingFS[0]);
                        }
                        else {
                            if (msg.data.MEMFS[0]) {
                                resolve(msg.data.MEMFS[0]);
                            }
                            else {
                                reject();
                            }
                        }
                        break;
                    default:
                        break;
                }
            };
        });
    }

    async _loadZip(url) {
        // Fetch the data zip file
        let response = await fetch(url);
        return await response.blob();
    }

    async _perform() {
        let filename = this.process.working[0].name;

        this._baseFS = this.process.working;

        // Load standard libraries
        if (!this._systemRoot) {
            this._systemRoot = await this._loadZip(`${this.process.basepath}static/${this.process.target}/root.zip`);
        }

        // Run 'gcc'
        let basename = (filename.match(/([^/]+)[.].+$/) || [null, filename])[1];
        this._workingFS = [];

        // TODO: when lto support can be enabled, drop the -fno-lto flag
        // lto support would have `ld` dlopen the ltoplugin shared library...
        // which we cannot easily do in JS land.
        let args = ["gcc", "-isystem", "/usr/include", "-isystem", "/usr/lib/gcc/riscv64-unknown-linux-gnu/10.2.0/include", "/input/" + filename, "-c", "-o", basename + ".o", "-I", "/input", "-fno-lto"];

        if (this.process instanceof Linker) {
            let linkerScriptFile = null;
            if (this.process.files[0]) {
                // Linker script file is here instead
                linkerScriptFile = "/input/" + this.process.files[0].name;
            }

            var files = this.process.working;
            args = files.map( (info) => info.name ).sort();
            args = ["gcc", "-o", this.process.project + this.suffix, "-fno-lto"].concat(args);
            if (linkerScriptFile) {
                args = args.concat(["-T", linkerScriptFile]);
            }
        }

        let result = await this._exec(filename, args);
        this.process.done();
        return result;
    }

    /**
     * Performs the compilation routine.
     *
     * @override
     */
    perform(resolve, reject) {
        try {
            (new Promise( async (innerResolve, innerReject) => {
                let ret = await this._perform();
                innerResolve(ret);
            })).then( (binary) => {
                resolve(binary);
            });
        }
        catch (e) {
            reject(e);
        }
    }
}

GCC.ERROR_REGEX = /^(\S+):(\d+):\s+Error:\s+(.+)$/;
GCC.DELEGATE_REGEX = /^__rawrs__: running program: (.+)$/;
