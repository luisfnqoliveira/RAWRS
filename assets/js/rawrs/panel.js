// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Tabs } from './ui/tabs';
import { EventComponent } from './event_component';

/**
 * Represents a runtime panel.
 */
export class Panel extends EventComponent {
    /**
     * Constructs the basic Panel along with the root element.
     *
     * That root element is then available as `this.element` within the panel
     * implementation.
     *
     * To have the panel be made available within the interface, you must add
     * an instance of the panel to the `options` of a Simulator or Debugger.
     */
    constructor(basepath, settings) {
        super();

        // Retain the basepath
        this.__basepath = basepath;

        // Retain settings
        this.__settings = settings;

        // Create the base element
        this.__element = document.createElement("div");
        this.element.classList.add("run-panel");
    }

    /**
     * Returns the base path for the application.
     */
    get basepath() {
        return this.__basepath;
    }

    /**
     * Returns the initial settings.
     */
    get settings() {
        return this.__settings;
    }

    /**
     * Returns the root element of the panel.
     *
     * @returns {HTMLElement} The element containing the panel.
     */
    get element() {
        return this.__element;
    }

    /**
     * The localization key or string serving as the name of this panel.
     */
    get name() {
        return this.settings.name || "Panel";
    }

    /**
     * Whether or not this panel is currently visible.
     */
    get visible() {
        return this.__visible;
    }

    /**
     * Returns the tab panel holding the panel element.
     */
    get tabPanel() {
        let current = this.element;
        while (current && !current.classList.contains('tab-panel')) {
            current = current.parentNode;
        }

        return current;
    }

    /**
     * Loads the panel.
     *
     * Called when the panel is added to the interface.
     */
    load() {
        // Do nothing when unimplemented
    }

    /**
     * Called when the panel is now visible.
     */
    shown() {
        // Do nothing when unimplemented
    }

    /**
     * Called when the panel is no longer visible.
     */
    hidden() {
        // Do nothing when unimplemented
    }
}
