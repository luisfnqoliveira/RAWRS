// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

/**
 * Represents a step in the processing stage.
 *
 * A Step holds the state and status of a processing step. For instance, a
 * compilation process might have a preprocessing, compilation, and assembly
 * step. Each of these has its own errors, standard out, output file, and
 * general status.
 *
 * Every step is marked within the Console. Updating this instance will update
 * the UI reflecting the step and process overall in the UI Console panel.
 */
export class Step extends EventComponent {
    /**
     * Creates a new instance of a processing step.
     */
    constructor(element, description, output) {
        super();

        this.__element = element;
        this.__status = Step.STATUS_PENDING;
        this.__output = element.querySelector("pre");

        this.bindEvents();
    }

    /**
     * Returns the element associated with this step.
     *
     * @returns {HTMLElement} The element for this step.
     */
    get element() {
        return this.__element;
    }

    /**
     * Updates the status of the step to the given value.
     *
     * The `value` parameter can be `Step.STATUS_SUCCESS` or
     * `Step.STATUS_FAILURE`. This UI will reflect the change in status as
     * appropriate if the step has been reported to the Console.
     *
     * @param {number} value - The new status for the step.
     */
    set state(value) {
        this.__status = value;

        let tag = ["pending", "done", "failed"][value];
        this.element.setAttribute('data-status', tag);

        if (tag === "done") {
            this.element.querySelector("h2").classList.remove("expanded");
        }
    }

    /**
     * Expand the step contents.
     */
    bindEvents() {
        let header = this.element.querySelector("h2");
        header.addEventListener("click", (event) => {
            header.classList.toggle("expanded");
        });
    }

    /**
     * Marks this step as successful.
     */
    done() {
        this.state = Step.STATUS_SUCCESS;
    }

    /**
     * Marks this step as failed.
     */
    fail() {
        this.state = Step.STATUS_FAILURE;
    }

    /**
     * Writes output to this step.
     */
    write(data) {
        this.__output.textContent += data;
    }

    /**
     * Writes a line of output to this step.
     */
    writeln(data) {
        this.write(data);
        this.write("\n");
    }
}

/**
 * The Step is ongoing.
 */
Step.STATUS_PENDING = 0;

/**
 * The Step has completed successfully.
 */
Step.STATUS_SUCCESS = 1;

/**
 * The Step has completed, but has failed.
 */
Step.STATUS_FAILURE = 2;
