// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component.js';

/**
 * This represents the text editor component for the user interface.
 */
export class Editor extends EventComponent {
    /**
     * Constructs the editor instance given the id to find the `textarea`.
     *
     * This will convert the `<textarea>` tag it finds to the sophisticated
     * editor instance.
     *
     * @param {String} id - The id to use to find the `textarea`.
     */
    constructor(id) {
        super();

        // Create an Ace Editor instance
        this._editor = window.ace.edit(id);

        // Set theme
        this._editor.setTheme("ace/theme/monokai");

        // Add the editor icon to its place
        let icon = document.body.querySelector("svg.editor-icon");
        let scroller = document.body.querySelector(".ace_scroller");
        if (scroller && icon) {
            let content = scroller.querySelector(".ace_content");

            if (content) {
                scroller.insertBefore(icon, content);
            }
        }

        // Add the margins
        this._editor.setShowPrintMargin(false);

        // Have gutter (line numbers) be as wide as it needs to be instead of
        // getting wider as you hit line 100, 1000, etc.
        this._editor.setOption('fixedWidthGutter', true);

        // Enable autocomplete
        this._editor.setOptions({
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
        });

        this._editor.getSession().on('change', () => {
            this.trigger('change');
        });

        // Set the default 'mode' for highlighting
        this._editor.session.setMode("ace/mode/assembly_riscv");

        // Assign the completer
        var langTools = window.ace.require("ace/ext/language_tools");
        let completer = {
            getCompletions: (editor, session, pos, prefix, callback) => {
                if (this.target) {
                    let line = session.getLine(pos.row);
                    let linePrefix = line.substring(0, pos.column - prefix.length).trim();
                    var wordRange = session.getWordRange(pos.row, pos.column);
                    var word = session.getTextRange(wordRange);
                    var token = session.getTokenAt(pos.row, pos.column);
                    let kind = "comment";
                    if (token) {
                        kind = token.type;
                    }
                    callback(null, this.target.onCompletion(this, line, linePrefix, word, kind, pos.row, pos.column));
                }
                else {
                    callback(null, []);
                }
            },
            getDocTooltip: (item) => {}
        };

        // Set the completer to our completer
        this._editor.completers = [completer];

        // Now, we look at supporting instruction 'popovers'
        // Hook into the mousemove event to track the mouse position:
        this._editor.on("mousemove", (event) => {
            // Get the position within the document
            var pos = event.getDocumentPosition();

            // Get the word at this position
            var wordRange = this._editor.session.getWordRange(pos.row, pos.column);
            var word = this._editor.session.getTextRange(wordRange);
            let line = this._editor.session.getLine(pos.row);
            var token = this._editor.session.getTokenAt(pos.row, pos.column);
            let kind = "comment";
            if (token) {
                kind = token.type;
            }

            // Get word screen position (relative to top-left of editor)
            var startPoint = this._editor.renderer.$cursorLayer.getPixelPosition(
                wordRange.start, false // Ace does not seem to use this boolean.
            );                         // We will keep it false just in case.
            var endPoint = this._editor.renderer.$cursorLayer.getPixelPosition(
                wordRange.end, false
            );

            // Get editor position
            let editorNode = document.querySelector("pre.ace_editor");
            let editorBounds = editorNode.getBoundingClientRect();
            let left = startPoint.left + editorBounds.x;
            let top = startPoint.top + editorBounds.y;
            let end = endPoint.left + editorBounds.x;

            // We need to account for the gutter
            left += this._editor.renderer.gutterWidth;
            end += this._editor.renderer.gutterWidth;

            // We need to account for the editor scroll
            top -= this._editor.renderer.scrollTop;

            // We need to account for instructions that appear at the end of the line (like ecall)
            // which will popover as long as the mouse moves over the same row
            if (end < event.x) {
                word = "";
            }

            if (this.target) {
                if (!this._last || this._last[0] !== line || this._last[1] !== word) {
                    this._last = [line, word, kind];
                    this.target.onMouseover(this, line, word, kind, left, top);
                }
            }
        });

        // When the mouse leaves the editor, hide the tooltip
        this._editor.container.addEventListener("mouseout", this.hideTooltip.bind(this));

        // Inserts any new labels the user added when the cursor changes lines
        this._editor.session.selection.on('changeCursor', (e) => {
            // Get the properties of this cursor
            let pos = this._editor.getCursorPosition();
            let line = this._editor.session.getLine(pos.row);
            var wordRange = this._editor.session.getWordRange(pos.row, pos.column);
            var word = this._editor.session.getTextRange(wordRange);
            var token = this._editor.session.getTokenAt(pos.row, pos.column);
            let kind = "comment";
            if (token) {
                kind = token.type;
            }

            let info = {
                row: pos.row,
                column: pos.column,
                line: line,
                word: word,
                kind: kind
            };

            if (!this._lastCursorInfo) {
                this._lastCursorInfo = info;
            }

            if (this.target) {
                this.target.onPosition(this, pos.row, pos.column, line, word, kind, this._lastCursorInfo);
            }

            this._lastCursorInfo = info;
        });
    }

    /**
     * Retrieves the text in the current document.
     */
    get value() {
        return this._editor.getValue();
    }

    /**
     * Retrieves the current annotations found within the current document.
     */
    get annotations() {
        return this._editor.getSession().getAnnotations();
    }

    /**
     * Sets the annotations that will appear in the current document.
     */
    set annotations(value) {
        this._editor.getSession().setAnnotations(value);

        if (!value) {
            this._editor.getSession().clearAnnotations();
        }
    }

    /**
     * Retrieves the current target that is being used.
     */
    get target() {
        return this._target;
    }

    /**
     * Sets the current target for the document.
     */
    set target(value) {
        this._target = value;
    }

    /**
     * Loads the given text and sets the filename for the current document.
     */
    load(text, filename) {
        filename ||= "input.s";

        // Get the mode from the defaults
        let mode = "c_cpp";
        if (filename.endsWith(".s")) {
            // Set the 'mode' for highlighting
            mode = "assembly_x86";
        }
        else if (filename.endsWith(".c") || filename.endsWith(".cc") || filename.endsWith("cpp")) {
            mode = "c_cpp";
        }

        // -1 moves the cursor to the start (without this,
        // it will select the entire text... I dunno)
        this._editor.setValue(text, -1);
        this._editor.getSession().setUndoManager(new window.ace.UndoManager());

        // Tell the target (and allow it to override the highlighting mode)
        if (this.target) {
            let altmode = this.target.onLoad(text, filename);
            if (altmode) {
                mode = altmode;
            }
        }

        // Set the 'mode' for highlighting
        this._editor.session.setMode("ace/mode/" + mode);

        // Force recompute of the cursor info
        this._lastCursorInfo = null;
    }

    /**
     * Focus on the text editor.
     */
    focus() {
        this._editor.focus();
    }

    /**
     * Remove any focus on this text editor.
     */
    blur() {
        this._editor.blur();
    }

    /**
     * A helper to hide any displayed tooltip or popover.
     */
    hideTooltip() {
        let tooltipNode = Editor.tooltipNode;
        Editor.tooltipNode = null;
        if (tooltipNode && tooltipNode.parentNode) {
            tooltipNode.parentNode.removeChild(tooltipNode);
        }
    }

    /**
     * A helper to display the given html as a popover at the given location.
     *
     * @param {String} html - The HTML to add to the popover.
     * @param {number} x - The x position of the popover.
     * @param {number} y - The y position of the popover.
     */
    showTooltip(html, x, y) {
        if (!Editor.tooltipNode) {
            // Create a tooltip element
            Editor.tooltipNode = document.createElement("div");
            Editor.tooltipNode.classList.add("ace_tooltip");
            Editor.tooltipNode.classList.add("ace_doc-tooltip");
            Editor.tooltipNode.style.margin = 0;
            Editor.tooltipNode.style.pointerEvents = "none";
            Editor.tooltipNode.tabIndex = -1;
        }

        let tooltipNode = Editor.tooltipNode;
        tooltipNode.innerHTML = html;

        if (!tooltipNode.parentNode) {
            document.body.appendChild(tooltipNode);
        }

        // Position the node
        tooltipNode.style.bottom = (window.innerHeight - y) + 'px';
        tooltipNode.style.left = x + 'px';

        // Display the node
        tooltipNode.style.display = "block";
    }
}
