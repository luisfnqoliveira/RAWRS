// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component';

export class Toolbar extends EventComponent {
    /**
     * Create a new instance of a toolbar for the given root element.
     *
     * @param {HTMLElement} root The element to look for the toolbar within.
     */
    constructor(root) {
        super();

        let element = root.querySelector(".toolbar");
        this._element = element;

        element.querySelectorAll(":scope button").forEach( (button) => {
            button.addEventListener("click", (event) => {
                this.trigger('click', button);
            });
        });
    }

    /**
     * Returns the element associated with this toolbar.
     *
     * @returns {HTMLElement} The element for this toolbar.
     */
    get element() {
        return this._element;
    }

    /**
     * Sets the status of the given button.
     *
     * @param {String} button The button identifier.
     */
    setStatus(button, status) {
        let buttonElement = this.element.querySelector("button#" + button);

        if (buttonElement) {
            if (status) {
                buttonElement.setAttribute("data-status", status);

                if (status === "disabled") {
                    buttonElement.setAttribute("disabled", "");
                    if (button === "step") {
                        buttonElement.setAttribute("title", buttonElement.getAttribute("data-i18n-pause"));
                    }
                }
                else {
                    buttonElement.removeAttribute("disabled");
                    if (button === "step") {
                        buttonElement.setAttribute("title", buttonElement.getAttribute("data-i18n-step"));
                    }
                }
            }
            else {
                buttonElement.removeAttribute("data-status");
                buttonElement.removeAttribute("disabled");
            }
        }
    }

    /**
     * Retrieves the status of the specified button or undefined if not set.
     *
     * @param {String} button The button identifier.
     * @returns String The status.
     */
    getStatus(button) {
        let buttonElement = this.element.querySelector("button#" + button);

        if (buttonElement) {
            return buttonElement.getAttribute("data-status");
        }

        return undefined;
    }
}
