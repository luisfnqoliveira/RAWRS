// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { Tabs }           from './tabs';
import { Step }           from './step';
import { Util }           from '../util';
import { EventComponent } from '../event_component';

/**
 * This widget represents the console output which shows processing steps.
 *
 * Each item in the console is a "Process" where it represents, generally, a
 * running task that takes some file and produces another file. For instance, a
 * compiler Process step can take a "`code.c`" file and yield a "`code.o`" file
 * as the final result.
 *
 * Each Process can have multiple steps. Generally it has only one, but it
 * might have a few. For instance, the compilation noted above might involve
 * several actual steps (maybe invoking multiple process runs). It might need
 * to first convert "`code.c`" into "`code.i`" with a preprocessor. Then it may
 * convert that to "`code.s`" due to the compiler pass producing assembly. Then,
 * it may complete the process by assembling this into "`code.o`", the final
 * product.
 *
 * Each of these can be listed as a step.
 *
 * To do this, you invoke `newProcess` to make the outer container for the
 * process and all individual steps. Then invoking `newStep` with that returned
 * process will add those steps. Whenever you would like to add general output
 * to the step, which can be expanded and read within the console if desired,
 * you invoke the `write`/`writeln` functions and pass along either the element
 * returned by `newProcess` or `newStep` depending on where you would like the
 * output to be displayed.
 */
export class Console extends EventComponent {
    /**
     * Create a new instance of a terminal for the given root element.
     *
     * @param {HTMLElement} root The element to look for the terminal within.
     */
    constructor(root) {
        super();

        let element = root.querySelector(".output-console ol.output-processes");
        this._element = element;

        // Get template for producing steps
        this._processTemplate = root.querySelector('template.process');
        this._stepTemplate = root.querySelector('template.step');

        this._tabs = Tabs.load(document.querySelector('#main-tabs'));
        if (this._tabs) {
            this._tabs.on('change', (button) => {
                this.updateActivePanel();
            });
        }

        // Initially ensure the active panel has the console
        this.updateActivePanel();
    }

    /**
     * Returns the element associated with this terminal.
     *
     * @returns {HTMLElement} The element for this terminal.
     */
    get element() {
        return this._element;
    }

    /**
     * Clears the terminal.
     */
    clear() {
        this.element.innerHTML = "";
    }

    /**
     * Ensures the console appears on the active panel.
     */
    updateActivePanel() {
        // Terminal the new tab
        let button = this._tabs.element.querySelector(".active button");

        // Bail if there is no active tab
        if (!button) {
            return;
        }

        let panelId = button.getAttribute('aria-controls');
        let assembleContainer = document.querySelector('#assemble-console-panel');
        let runContainer = document.querySelector('#run-console-panel');

        // Copy the console to the appropriate tab and maintain the scroll.
        if (panelId === 'assemble-panel') {
            let outputs = runContainer.querySelector("ol.output-processes");
            if (outputs && assembleContainer) {
                let scroll = outputs.scrollTop;
                let gotoEnd = scroll == outputs.scrollTopMax;
                assembleContainer.appendChild(outputs);
                outputs.scrollTop = scroll;
                if (gotoEnd) {
                    outputs.scrollTop = outputs.scrollTopMax;
                }
            }
        }
        else if (panelId === 'run-panel') {
            let outputs = assembleContainer.querySelector("ol.output-processes");
            if (outputs && runContainer) {
                let scroll = outputs.scrollTop;
                let gotoEnd = scroll == outputs.scrollTopMax;
                runContainer.appendChild(outputs);
                outputs.scrollTop = scroll;
                if (gotoEnd) {
                    outputs.scrollTop = outputs.scrollTopMax;
                }
            }
        }
    }

    /**
     * Updates to show a new process of the processing run.
     *
     * When `filename` is given, it will add a visual cue to the file being
     * produced by this step, ultimately.
     *
     * @param {string} description - The title of the process.
     * @param {string|null} filename - The name of the file (if any)
     *
     * @returns {HTMLElement} The element representing the process.
     */
    newProcess(description, filename = null) {
        // Create the step element from the template
        let element = Util.createElementFromTemplate(this._processTemplate);

        // Update description
        let span = element.querySelector("h1 > span.description");
        span.textContent = description;

        // Update input file
        let inputSpan = element.querySelector("h1 > span.filename");
        if (filename) {
            inputSpan.textContent = filename;
        }
        else {
            inputSpan.setAttribute('aria-hidden', 'true');
            inputSpan.setAttribute('hidden', '');
        }

        // Append it
        this.element.appendChild(element);

        // Bind events
        let header = element.querySelector("h1");
        header.addEventListener("click", (event) => {
            header.classList.toggle("expanded");
        });

        // Return the reference to it.
        return element;
    }

    /**
     * Creates a new sub-step of the processing run.
     *
     * @param {HTMLElement} process - Element in the console for the process.
     * @param {string} description - A short one-word description of the step.
     * @param {string} output - The name of the file being produced.
     *
     * @returns {Step} The step instance.
     */
    newStep(process, description, output) {
        // Create the step element from the template
        let element = Util.createElementFromTemplate(this._stepTemplate);

        // Update description
        let span = element.querySelector("h2 > span.description");
        span.textContent = description;

        // Update output file
        let outputSpan = element.querySelector("h2 > span.output-filename");
        if (output) {
            outputSpan.textContent = output;
        }
        else {
            outputSpan.setAttribute('aria-hidden', 'true');
            outputSpan.setAttribute('hidden', '');
        }

        // Add it to the application view
        process.querySelector("ol.output-process-steps").appendChild(element);

        // Create the Step instance
        let step = new Step(element, description, output);

        // Return it
        return step;
    }

    /**
     * Writes output to the given process.
     *
     * @param {HTMLElement} process - The list item representing the process.
     * @param {string} data - The string to write.
     */
    write(process, data) {
        if (!process || !process.querySelector) {
            return;
        }

        let pre = process.querySelector("pre.output-process-stdout");
        let outputs = this.element;
        let gotoEnd = outputs.scrollTop == outputs.scrollTopMax;
        if (pre) {
            if (data === "\n") {
                // We only write an empty line if there are other lines here in the pre
                if (pre.textContent.trim() === "") {
                    return;
                }
            }
            pre.textContent += data;
        }

        // Maintain auto-scroll
        if (gotoEnd) {
            outputs.scrollTop = outputs.scrollTopMax;
        }
    }

    /**
     * Writes the output followed by a newline.
     *
     * @param {HTMLElement} process - The list item representing the process.
     * @param {String} data - The string to write.
     */
    writeln(process, data) {
        this.write(process, data);
        this.write(process, "\n");
    }

    /**
     * Marks the process as being completed successfully.
     *
     * Optionally, you can tell it whether or not to collapse the output steps
     * of the process in the console. By default, it will collapse these.
     *
     * @param {HTMLElement} process - The list item representing the process.
     * @param {boolean} collapse - When `true`, collapses the steps.
     */
    done(process, collapse = true) {
        process.setAttribute("data-status", "done");
        if (collapse) {
            process.querySelector("h1").classList.remove("expanded");
        }
    }

    /**
     * Marks the process as being completed in failure.
     *
     * @param {HTMLElement} process - The list item representing the process.
     */
    fail(process) {
        process.setAttribute("data-status", "failed");
    }
}
