// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from '../event_component.js';

import dialogPolyfill from 'dialog-polyfill';

export class Dialog extends EventComponent {
    /**
     * Create a new dialog instance for the dialog at the given id.
     *
     * @param {string} id The identifier for the `<dialog>` element.
     */
    constructor(id) {
        super();

        // Ensure the id is in the form of a css selector
        if (id[0] != "#") {
            id = "#" + id;
        }

        // Find the dialog element
        this._id = id;
        this._dialog = document.querySelector(this._id);

        // Ensure that the polyfill happens, if it is necessary
        dialogPolyfill.registerDialog(this._dialog);

        // Bind events for the common dialog elements
        this.bindDialogEvents();
    }

    /**
     * The identifier of the dialog that this class represents.
     *
     * @returns {string} The identifier.
     */
    get id() {
        return this._id;
    }

    /**
     * Returns the `<dialog>` element this class represents.
     *
     * @returns {HTMLElement} The wrapped element.
     */
    get dialog() {
        return this._dialog;
    }

    /**
     * Displays the dialog.
     */
    open() {
        this._dialog.showModal();
        this._dialog.focus();
        this.trigger('open');
    }

    /**
     * Closes the dialog.
     */
    close() {
        this._dialog.close();
        this.trigger('close');
    }

    /**
     * Submits the dialog's form.
     */
    submit(form) {
        this.close();
    }

    /**
     * Binds events, which is done internally when the dialog is initialized.
     */
    bindDialogEvents() {
        // Bail if this was called before
        if (this._dialog.classList.contains("dialog-default-bound")) {
            return;
        }

        // Do not allow the events to be bound twice
        this._dialog.classList.add("dialog-default-bound");

        // Close button
        let closeButton = this.dialog.querySelector('button.close');
        closeButton.addEventListener('click', (event) => {
            this.close();
        });

        let form = this.dialog.querySelectorAll('form');
        form.forEach( (formElement) => {
            formElement.addEventListener('submit', (event) => {
                event.preventDefault();
                this.submit(formElement);
            });
        });
    }
}
