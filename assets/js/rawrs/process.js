// RAWRS - Robust Assembler and Web-based Runtime System
// Copyright (C) 2017-2022 wilkie
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { EventComponent } from './event_component';

/**
 * The base class to wrap any program used in the compilation stages.
 */
export class Process extends EventComponent {
    constructor(console, project, basepath, target) {
        super();

        // Initially, there are no errors
        this.clearErrors();

        // Set the console this is connected to.
        this.__console = console;

        // The list of files to mount
        this.__filesLookup = {};
        this.__files = [];

        // The files we have simply seen
        this.__seen = [];

        // The working directory is initially empty
        this.__working = [];

        // The name of the project
        this.__project = project;

        // Retain the basepath
        this.__basepath = basepath;

        // Retain the target
        this.__target = target;

        // Set initial state
        this.__state = Process.STATE_PENDING;
    }

    /**
     * Retrieves the application base path for building URLs.
     */
    get basepath() {
        return this.__basepath;
    }

    /**
     * Retrieves the name of the project.
     */
    get project() {
        return this.__project;
    }

    /**
     * Retrieves the name of this target.
     */
    get target() {
        return this.__target.name;
    }

    /**
     * Retrieves the options.
     */
    get options() {
        return this.constructor.options;
    }

    /**
     * Retrieves the options.
     */
    static get options() {
        return {};
    }

    /**
     * Retrieves the console to write messages to.
     */
    get console() {
        return this.__console;
    }

    /**
     * Retrieves a set of blobs for uploaded files in '/input'.
     */
    get files() {
        return this.__files;
    }

    /**
     * Retrieves a set of file data for files added to '/work' via `add`.
     */
    get working() {
        return this.__working;
    }

    /**
     * Retrieves the errors gathered in all assembles so far.
     */
    get errors() {
        return this.__errors;
    }

    /**
     * Purges all record of assemble errors and warnings.
     */
    clearErrors() {
        this.__errors = [];
    }

    /**
     * Purges all uploaded files to the '/input' path.
     */
    clearFiles() {
        this.__filesLookup = {};
        this.__files = [];
    }

    /**
     * Purges all files in the working directory.
     */
    clearWorking() {
        this.__working = [];
    }

    /**
     * Adds a read-only text file to the file-system under '/input'.
     *
     * @param {string} name - The file name to add to the '/input' directory.
     * @param {string} source - The source text.
     */
    upload(name, source) {
        // Ensure it has a newline at the end
        source = source + "\n";

        // Create a blob
        let fileData = new Blob([source], {'type': 'text/plain'});

        this.__filesLookup[name] = {
            name: name,
            data: fileData
        };
        this.__files.push(this.__filesLookup[name]);
    }

    /**
     * Retrieves the file data for the given file uploaded via `upload`.
     */
    download(name) {
        return this.__filesLookup[name];
    }

    /**
     * Simply notifies this process that a file exists with a particular name.
     */
    notify(name) {
        this.__seen.push(name);
    }

    /**
     * Returns the files that were seen by `notify`.
     */
    get seen() {
        return this.__seen;
    }

    /**
     * Adds a working space file under '/work'.
     */
    add(object) {
        this.__working.push(object);
    }

    get state() {
        return this.__state;
    }

    set state(value) {
        this.__state = value;

        // Set the step state as well
        this.step.state = value;
    }

    get step() {
        if (!this.__step) {
            // Create a generic step for this process.
            this.__step = this.console.newStep(this.__process, this.__description);
        }

        return this.__step;
    }

    write(data) {
        this.step.write(data);
    }

    writeln(data) {
        this.write(data);
        this.write("\n");
    }

    /**
     * Performs the process.
     *
     * Calling `reject` will throw an exception for the error.
     *
     * This function should be overloaded to provide the functionality of the
     * process.
     *
     * @param {function} resolve - The callback function upon success.
     * @param {function} reject - The callback function upon failure.
     * @param {Object} options - A set of options for this specific invocation.
     */
    perform(resolve, reject, options) {
        throw new Error("Unimplemented process");
    }

    /**
     * Invokes the process.
     */
    async invoke(options = {}) {
        return new Promise( (resolve, reject) => {
            this.perform(resolve, reject, options);
        });
    }

    get process() {
        return this.__process;
    }

    newProcess(description, filename) {
        this.__process = this.console.newProcess(description, filename);
        this.__description = description;
        this.__filename = filename;
    }

    get description() {
        return "Processing";
    }

    updateHeader(description, filename) {
        if (!this.__process) {
            this.newProcess(description, filename);
        }

        // TODO: update the process header

        this.__description = description;
        this.__filename = filename;
    }

    newStep(description, output = null) {
        this.__step = this.console.newStep(this.__process, description, output);
        return this.__step;
    }

    done() {
        this.state = Process.STATE_SUCCESS;
        this.console.done(this.__process);
    }

    fail() {
        this.state = Process.STATE_FAILURE;
        this.console.fail(this.__process);
    }
}

Process.STATE_PENDING = 0;
Process.STATE_SUCCESS = 1;
Process.STATE_FAILURE = 2;
