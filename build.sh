#!/bin/bash

# Build targets
./scripts/riscv64/build-all.sh

# Build assets
npm run compile-assets

# Build JavaScript application
npm run build
